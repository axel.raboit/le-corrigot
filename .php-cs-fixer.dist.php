<?php

/*
 * (c) No name
 */

$fileHeaderComment = <<<'COMMENT'
(c) No name
COMMENT;

$finder = PhpCsFixer\Finder::create()
    ->in(__DIR__)
    ->exclude(['config', 'public', 'src', 'translations', 'var', 'vendor'])
    ->notPath('public/adminer.php')
;

$config = new PhpCsFixer\Config();
$config
    ->setRiskyAllowed(true)
    ->setRules(
        [
            '@PSR12' => true,
            '@PSR2' => true,
            '@PhpCsFixer' => true,
            '@Symfony' => true,
            '@Symfony:risky' => true,
            'header_comment' => ['header' => $fileHeaderComment, 'separate' => 'both'],
            'linebreak_after_opening_tag' => true,
            'mb_str_functions' => true,
            'no_php4_constructor' => true,
            'no_unreachable_default_argument_value' => true,
            'no_useless_else' => true,
            'no_useless_return' => true,
            'php_unit_strict' => true,
            'phpdoc_order' => true,
            'strict_comparison' => true,
            'strict_param' => true,
            'blank_line_between_import_groups' => false,
        ]
    )
    ->setCacheFile(__DIR__.'/.php-cs-fixer.cache')
    ->setFinder($finder)
    ->setLineEnding("\n")
;

return $config;
