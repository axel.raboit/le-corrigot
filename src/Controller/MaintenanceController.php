<?php

declare(strict_types=1);

/*
 * (c) No name
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MaintenanceController extends AbstractController
{
    #[Route(
        path: '/maintenance',
        name: 'maintenance',
        priority: 1,
    )]
    public function maintenance(): Response
    {
        $elements = [];

        return $this->render('page/page-maintenance.html.twig', $elements);
    }
}
