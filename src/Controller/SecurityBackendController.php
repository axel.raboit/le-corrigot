<?php

declare(strict_types=1);

/*
 * (c) No name
 */

namespace App\Controller;

use App\Entity\Page;
use App\Enum\DataEnum;
use App\Manager\DataEnumManager;
use App\Repository\PageRepository;
use App\Service\PageService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityBackendController extends AbstractController
{
    private PageService $pageService;
    /* private PageRepository $pageRepository; */
    private DataEnumManager $dataEnumManager;

    public function __construct(
        PageService $pageService,
        /* PageRepository $pageRepository, */
        DataEnumManager $dataEnumManager,
    ) {
        $this->pageService = $pageService;
        /* $this->pageRepository = $pageRepository; */
        $this->dataEnumManager = $dataEnumManager;
    }

    #[Route('/backend/connexion', name: 'backend_login')]
    public function backendLogin(AuthenticationUtils $authenticationUtils, Request $request, Page $page): Response
    {
        /* TODO: Improve the condition with voters */
        if ($this->getUser()) {
            return $this->redirectToRoute('home');
        }

        /* TODO: Improve the condition with voters */
        $backendLoginActivation = $this->dataEnumManager->getDataEnumValue(DataEnum::DATA_BACKEND_LOGIN_ACTIVATION_DEV_KEY);

        if (false === $backendLoginActivation) {
            return $this->redirectToRoute('home');
        }

        /* $page = $this->pageRepository->getPageFromDataDevKey(DataEnum::DATA_PAGE_BACKEND_LOGIN_DEV_KEY); */

        $error = $authenticationUtils->getLastAuthenticationError();

        $lastUsername = $authenticationUtils->getLastUsername();

        $elements = $this->pageService->getPageElements($page, $request);

        $elements['last_username'] = $lastUsername;
        $elements['error'] = $error;

        return $this->render($elements['template'], $elements);
    }

    #[Route(path: '/backend/logout', name: 'backend_logout', priority : 100)]
    public function logout(): void
    {
        /* throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.'); */
    }
}
