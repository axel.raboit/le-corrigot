<?php

declare(strict_types=1);

/*
 * (c) No name
 */

namespace App\Controller;

use App\Entity\Page;
use App\Service\PageGalleryService;
use App\Service\PageService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LoftController extends AbstractController
{
    private PageService $pageService;
    private PageGalleryService $pageGalleryService;

    public function __construct(
        PageService $pageService,
        PageGalleryService $pageGalleryService,
    ) {
        $this->pageService = $pageService;
        $this->pageGalleryService = $pageGalleryService;
    }

    #[Route(
        path: '/loft',
        name: 'loft',
    )]
    public function loft(Request $request, Page $page): Response
    {
        $elements = $this->pageService->getPageElements($page, $request);

        $elements['gallery'] = $this->pageGalleryService->getPageGalleryElements($page, $request);

        return $this->render($elements['template'], $elements);
    }
}
