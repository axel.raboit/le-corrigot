<?php

declare(strict_types=1);

/*
 * (c) No name
 */

namespace App\Controller;

use App\Repository\PageRepository;
use App\Service\PageService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    private PageService $pageService;
    private PageRepository $pageRepository;

    public function __construct(
        PageService $pageService,
        PageRepository $pageRepository,
        ) {
        $this->pageService = $pageService;
        $this->pageRepository = $pageRepository;
    }

    #[Route('/', name: 'home', priority: 2)]
    public function index(Request $request): Response
    {
        if (false === $this->isGranted('CAN_ACCESS_WHEN_MAINTENANCE_IS_ACTIVATED')) {
            return $this->redirectToRoute('maintenance');
        }

        $elements = $this->pageService->getPageElements(
            $this->pageRepository->findOneBy(['title' => 'Accueil']),
            $request
        );

        return $this->render($elements['template'], $elements);
    }
}
