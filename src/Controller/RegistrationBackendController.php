<?php

declare(strict_types=1);

/*
 * (c) No name
 */

namespace App\Controller;

use App\Entity\Page;
use App\Entity\UserBackend;
use App\Enum\DataEnum;
use App\Form\RegistrationBackendFormType;
use App\Manager\DataEnumManager;
use App\Repository\PageRepository;
use App\Security\BackendLoginAuthenticator;
use App\Service\PageService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;

class RegistrationBackendController extends AbstractController
{
    private PageService $pageService;
    private PageRepository $pageRepository;
    private DataEnumManager $dataEnumManager;

    public function __construct(
        PageService $pageService,
        PageRepository $pageRepository,
        DataEnumManager $dataEnumManager,
    ) {
        $this->pageService = $pageService;
        $this->pageRepository = $pageRepository;
        $this->dataEnumManager = $dataEnumManager;
    }

    /* REGISTRATION IS ONLY ACTIVATED WHEN IT'S NECESSARY */
    #[Route(
        path: '/backend/inscription',
        name: 'backend_register',
    )]
    public function register(
        Request $request,
        Page $page,
        UserPasswordHasherInterface $userPasswordHasher,
        UserAuthenticatorInterface $userAuthenticator,
        BackendLoginAuthenticator $authenticator,
        EntityManagerInterface $entityManager,
    ): Response {
        /* TODO: Improve the condition with voters */
        if ($this->getUser()) {
            return $this->redirectToRoute('home');
        }

        /* TODO: Improve the condition with voters */
        $registrationActivation = $this->dataEnumManager->getDataEnumValue(DataEnum::DATA_BACKEND_REGISTRATION_ACTIVATION_DEV_KEY);

        if (false === $registrationActivation) {
            return $this->redirectToRoute('home');
        }

        /* $page = $this->pageRepository->getPageFromDataDevKey(DataEnum::DATA_PAGE_BACKEND_REGISTER_DEV_KEY); */

        $elements = $this->pageService->getPageElements($page, $request);

        $user = new UserBackend();
        $form = $this->createForm(RegistrationBackendFormType::class, $user, [
            'lang' => $request->getLocale(),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );
            $user->setRoles(['ROLE_BACKEND']);

            $entityManager->persist($user);
            $entityManager->flush();
            // do anything else you need here, like send an email

            return $userAuthenticator->authenticateUser(
                $user,
                $authenticator,
                $request
            );
        }

        $elements['registrationForm'] = $form->createView();

        return $this->render($elements['template'], $elements);
    }

    #[Route(
        path: '/backend/inscription/politique-d-acceptation-d-enregistrement',
        name: 'backend_policy_acceptance_registration',
    )]
    public function privacyPolicy(Request $request): Response
    {
        $page = $this->pageRepository->getPageFromDataDevKey(DataEnum::DATA_PAGE_REGISTRATION_POLICY_ACCEPTANCE_REGISTRATION_DEV_KEY);

        $elements = $this->pageService->getPageElements($page, $request);

        return $this->render($elements['template'], $elements);
    }
}
