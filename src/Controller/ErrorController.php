<?php

declare(strict_types=1);

/*
 * (c) No name
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\ErrorHandler\Exception\FlattenException;
use Symfony\Component\HttpFoundation\Response;

class ErrorController extends AbstractController
{
    public function show(
        FlattenException $exception,
    ): Response|FlattenException {
        $element = [];
        $element['template'] = 'exception/exception.html.twig';
        $element['exception'] = $exception;

        return $this->render($element['template'], $element);
    }
}
