<?php

declare(strict_types=1);

/*
 * (c) No name
 */

namespace App\Controller;

use App\Service\RouteService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LocaleController extends AbstractController
{
    #[Route('/change-locale', name: 'change_locale', priority: 2)]
    public function changeLocale(Request $request, RouteService $routeService): Response
    {
        $locale = $request->get('locale');
        $currentPath = $request->get('currentPath');
        $slug = $request->get('slug');

        $request->getSession()->set('_locale', $locale);

        if (null === $currentPath) {
            return $this->redirectToRoute(PageController::PAGE_DEFAULT, [
                '_locale' => $locale,
                'slug' => $slug,
            ]);
        }

        if (PageController::PAGE_DEFAULT !== $currentPath) {
            $routes = $routeService->getRoutes();
            $route = $routes[$currentPath];

            if (!mb_strstr($route->getPath(), '{slug}') && !mb_strstr($route->getPath(), '{id}')) {
                return $this->redirectToRoute($currentPath, [
                    '_locale' => $locale,
                ]);
            }

            return $this->redirectToRoute($currentPath, [
                '_locale' => $locale,
                'slug' => $slug,
            ]);
        }

        return $this->redirectToRoute($currentPath, [
            '_locale' => $locale,
            'slug' => $slug,
        ]);
    }
}
