<?php

/*
 * (c) No name
 */

namespace App\Controller\Admin;

use App\Entity\Image;
use App\Entity\Page;
use App\Entity\PageType;
use App\Entity\Website;
use App\Repository\PageRepository;
use App\Service\EasyAdminPageService;
use App\Service\PageService;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class PageCrudController extends AbstractCrudController
{
    private EasyAdminPageService $easyAdminPageService;
    private PageRepository $pageRepository;
    private PageService $pageService;

    public function __construct(
        EasyAdminPageService $easyAdminPageService,
        PageRepository $pageRepository,
        PageService $pageService,
    ) {
        $this->easyAdminPageService = $easyAdminPageService;
        $this->pageRepository = $pageRepository;
        $this->pageService = $pageService;
    }

    public static function getEntityFqcn(): string
    {
        return Page::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_EDIT, Action::INDEX)
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_EDIT, Action::DETAIL)
        ;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Page')
            ->setEntityLabelInPlural('Pages')
            ->setSearchFields([
                'id', 'title', 'titleEN', 'titleNL', 'name', 'nameEN', 'nameNL',
            ])
            ->setPaginatorPageSize(50)
            ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig')
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        yield IdField::new('id')->hideOnForm();

        yield TextField::new('title', 'Titre de la page');
        yield TextField::new('titleEN', 'Titre Anglais de la page')->hideOnIndex();
        yield TextField::new('titleNL', 'Titre Néerlandais de la page')->hideOnIndex();

        yield TextField::new('name', 'Nom de la page');
        yield TextField::new('nameEN', 'Nom Anglais de la page')->hideOnIndex();
        yield TextField::new('nameNL', 'Nom Néerlandais de la page')->hideOnIndex();

        yield TextField::new('slug', 'Slug de la page')
            ->setFormTypeOption('constraints', [
                new Callback(function ($value, ExecutionContextInterface $context) {
                    $entityId = null;
                    $slugExist = false;

                    $entityId = $context->getRoot()->getData()->getId();

                    $slugExist = $this->easyAdminPageService->isSlugExist($value, $entityId);

                    if (!preg_match('/^[a-z0-9\/-]+$/', $value)) {
                        $context->buildViolation('Le slug doit contenir uniquement des lettres minuscules, des tirets, chiffres, et des slashes.')
                            ->addViolation()
                        ;
                    }

                    if ('/' === $value[0]) {
                        $context->buildViolation('Le slug ne doit pas commencer par un slash.')
                            ->addViolation()
                        ;
                    }

                    if ('/' === $value[-1]) {
                        $context->buildViolation('Le slug ne doit pas finir par un slash.')
                            ->addViolation()
                        ;
                    }

                    if (true === $slugExist) {
                        $context->buildViolation('Le slug existe déjà.')
                            ->addViolation()
                        ;
                    }
                }),
            ])
        ;

        yield TextField::new('canonicalUrl', 'Url canonique de la page')
            ->hideOnIndex()
            ->setFormTypeOption('constraints', [
                new Callback(function ($value, ExecutionContextInterface $context) {
                    $entityId = null;
                    $slugExist = false;

                    $entityId = $context->getRoot()->getData()->getId();

                    $slugExist = $this->easyAdminPageService->isCanonicalUrlExist($value, $entityId);

                    if (null !== $value) {
                        if ('/' === $value[0]) {
                            $context->buildViolation('Le slug ne doit pas commencer par un slash.')
                                ->addViolation()
                            ;
                        }

                        if (true === $slugExist) {
                            $context->buildViolation('Le lien canonique existe déjà.')
                                ->addViolation()
                            ;
                        }
                    }
                }),
            ])
        ;

        yield AssociationField::new('type', 'Type de page')
            ->setRequired(false)
            ->setFormTypeOptions([
                'class' => PageType::class,
                'choice_label' => 'name',
            ])
        ;

        yield TextField::new('template', 'Template de la page');
        yield TextField::new('devCodeRouteName', 'Nom de la route');

        yield TextEditorField::new('preDescription', 'Pré-description de la page')->setFormType(CKEditorType::class)->hideOnIndex();
        yield TextEditorField::new('preDescriptionEN', 'Pré-description Anglaise de la page')->setFormType(CKEditorType::class)->hideOnIndex();
        yield TextEditorField::new('preDescriptionNL', 'Pré-description Néerlandaise de la page')->setFormType(CKEditorType::class)->hideOnIndex();

        yield TextEditorField::new('description', 'Description de la page')->setFormType(CKEditorType::class)->hideOnIndex();
        yield TextEditorField::new('descriptionEN', 'Description Anglaise de la page')->setFormType(CKEditorType::class)->hideOnIndex();
        yield TextEditorField::new('descriptionNL', 'Description Néerlandaise de la page')->setFormType(CKEditorType::class)->hideOnIndex();

        yield TextEditorField::new('contentPrimary', 'Contenu primaire de la page')->setRequired(false)->setFormType(CKEditorType::class)->hideOnIndex();
        yield TextEditorField::new('contentPrimaryEN', 'Contenu primaire Anglais de la page')->setRequired(false)->setFormType(CKEditorType::class)->hideOnIndex();
        yield TextEditorField::new('contentPrimaryNL', 'Contenu primaire Néerlandais de la page')->setRequired(false)->setFormType(CKEditorType::class)->hideOnIndex();

        yield TextEditorField::new('contentSecondary', 'Contenu secondaire de la page')->setRequired(false)->setFormType(CKEditorType::class)->hideOnIndex();
        yield TextEditorField::new('contentSecondaryEN', 'Contenu secondaire Anglais de la page')->setRequired(false)->setFormType(CKEditorType::class)->hideOnIndex();
        yield TextEditorField::new('contentSecondaryNL', 'Contenu secondaire Néerlandais de la page')->setRequired(false)->setFormType(CKEditorType::class)->hideOnIndex();

        yield TextEditorField::new('contentTertiary', 'Contenu tertiaire de la page')->setRequired(false)->setFormType(CKEditorType::class)->hideOnIndex();
        yield TextEditorField::new('contentTertiaryEN', 'Contenu tertiaire Anglais de la page')->setRequired(false)->setFormType(CKEditorType::class)->hideOnIndex();
        yield TextEditorField::new('contentTertiaryNL', 'Contenu tertiaire Néerlandais de la page')->setRequired(false)->setFormType(CKEditorType::class)->hideOnIndex();

        yield TextEditorField::new('contentQuaternary', 'Contenu quaternaire de la page')->setRequired(false)->setFormType(CKEditorType::class)->hideOnIndex();
        yield TextEditorField::new('contentQuaternaryEN', 'Contenu quaternaire Anglais de la page')->setRequired(false)->setFormType(CKEditorType::class)->hideOnIndex();
        yield TextEditorField::new('contentQuaternaryNL', 'Contenu quaternaire Néerlandais de la page')->setRequired(false)->setFormType(CKEditorType::class)->hideOnIndex();

        yield TextField::new('ctaTitle', 'Titre du bouton')->hideOnIndex();
        yield TextField::new('ctaTitleEN', 'Titre Anglais du bouton')->hideOnIndex();
        yield TextField::new('ctaTitleNL', 'Titre Néerlandais du bouton')->hideOnIndex();

        yield TextField::new('ctaText', 'Texte du bouton')->hideOnIndex();
        yield TextField::new('ctaTextEN', 'Texte Anglais du bouton')->hideOnIndex();
        yield TextField::new('ctaTextNL', 'Texte Néerlandais du bouton')->hideOnIndex();

        yield TextField::new('ctaUrl', 'Url du bouton')->hideOnIndex();
        yield IntegerField::new('weight', 'Poids de la page')->hideOnIndex();
        yield AssociationField::new('children', 'Page enfant')
            ->hideOnIndex()
            ->setRequired(false)
            ->setFormTypeOptions([
                'class' => Page::class,
                'choice_label' => 'title',
            ])
        ;
        yield AssociationField::new('parent', 'Page parent')
            ->hideOnIndex()
            ->setRequired(false)
            ->setFormTypeOption('disabled', true)
            ->setFormTypeOptions([
                'class' => Page::class,
                'choice_label' => 'title',
            ])
        ;
        yield TextField::new('bannerTitle', 'Titre de la bannière')->hideOnIndex();
        yield TextField::new('bannerTitleEN', 'Titre Anglais de la bannière')->hideOnIndex();
        yield TextField::new('bannerTitleNL', 'Titre Néerlandais de la bannière')->hideOnIndex();
        yield AssociationField::new('banner', 'Image principal')
            ->hideOnIndex()
            ->setRequired(false)
            ->setFormTypeOptions([
                'class' => Image::class,
                'choice_label' => 'title',
            ])
        ;
        yield AssociationField::new('ImageThumbnail', 'Image thumbnail')
            ->hideOnIndex()
            ->setRequired(false)
            ->setFormTypeOptions([
                'class' => Image::class,
                'choice_label' => 'title',
            ])
        ;
        yield TextField::new('category', 'Catégorie de la page')->hideOnIndex();
        yield AssociationField::new('website', 'Site web associé')
            ->hideOnIndex()
            ->setFormTypeOptions([
                'class' => Website::class,
                'choice_label' => 'name',
            ])
        ;
        yield TextEditorField::new('metaDescription', 'Meta description de la page')->hideOnIndex();
        yield TextEditorField::new('metaDescriptionEN', 'Meta description Anglaise de la page')->hideOnIndex();
        yield TextEditorField::new('metaDescriptionNL', 'Meta description Néerlandaise de la page')->hideOnIndex();
        yield IntegerField::new('devkey', 'Dev key')->setFormTypeOption('disabled', true);
        yield DateTimeField::new('createdAt', 'Créé le (heure en UTC)')->setFormTypeOption('disabled', true)->hideOnIndex();
        yield DateTimeField::new('updatedAt', 'Mis à jour le (heure en UTC)')->setFormTypeOption('disabled', true)->hideOnIndex();
        yield DateTimeField::new('deletedAt', 'Supprimé le (heure en UTC)')->setFormTypeOption('disabled', true)->hideOnIndex();
    }

    public function persistEntity(EntityManagerInterface $entityManager, mixed $entityInstance): void
    {
        $entityInstance->setCreatedAt(new \DateTimeImmutable());
        $entityInstance->setUpdatedAt(new \DateTimeImmutable());
        parent::persistEntity($entityManager, $entityInstance);
    }

    /* Set default values on the entity creation */
    public function createEntity(string $entityFqcn): Page
    {
        $page = new Page();

        $devkey = $this->pageRepository->getLastDevKey() + 1;

        if (null !== $devkey) {
            if (false === $this->pageService->isDevKeyUsed($devkey)) {
                $page->setDevKey($devkey);
            } else {
                throw new \Exception('DevKey already used');
            }
        }

        $page->setTemplate('page/page-simple.html.twig');

        return $page;
    }

    public function updateEntity(EntityManagerInterface $entityManager, mixed $entityInstance): void
    {
        /* SETTERS */

        /* We set the dev key if it's not set */
        if (null === $entityInstance->getDevKey()) {
            $devkey = $this->pageRepository->getLastDevKey() + 1;

            if (false === $this->pageService->isDevKeyUsed($devkey)) {
                $entityInstance->setDevKey($devkey);
            } else {
                throw new \Exception('DevKey already used');
            }
        }

        /* We add parent to the page that we've set child */
        foreach ($entityInstance->getChildren() as $child) {
            $child->setParent($entityInstance);
        }

        /* We remove parent from the page that we've removed child */
        $pages = $entityManager->getRepository(Page::class)->findAll();
        foreach ($pages as $page) {
            if ($page->getParent() === $entityInstance && !\in_array($page, $entityInstance->getChildren()->toArray(), true)) {
                $page->setParent(null);
            }
        }

        $entityInstance->setUpdatedAt(new \DateTimeImmutable());
        parent::updateEntity($entityManager, $entityInstance);
    }

    public function deleteEntity(EntityManagerInterface $entityManager, mixed $entityInstance): void
    {
        /* We remove parent reference from children */
        foreach ($entityInstance->getChildren() as $child) {
            $child->setParent(null);
        }

        $entityInstance->setDeletedAt(new \DateTimeImmutable());
        parent::deleteEntity($entityManager, $entityInstance);
    }
}
