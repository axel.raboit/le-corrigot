<?php

/*
 * (c) No name
 */

namespace App\Controller\Admin;

use App\Entity\PageType;
use App\Repository\PageTypeRepository;
use App\Service\EasyAdminPageTypeService;
use App\Service\PageTypeService;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class PageTypeCrudController extends AbstractCrudController
{
    private EasyAdminPageTypeService $easyAdminPageTypeService;
    private PageTypeRepository $pageTypeRepository;
    private PageTypeService $pageTypeService;

    public function __construct(
        EasyAdminPageTypeService $easyAdminPageTypeService,
        PageTypeRepository $pageTypeRepository,
        PageTypeService $pageTypeService,
    ) {
        $this->easyAdminPageTypeService = $easyAdminPageTypeService;
        $this->pageTypeRepository = $pageTypeRepository;
        $this->pageTypeService = $pageTypeService;
    }

    public static function getEntityFqcn(): string
    {
        return PageType::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_EDIT, Action::INDEX)
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_EDIT, Action::DETAIL)
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        yield IdField::new('id')->hideOnForm();
        yield TextField::new('name', 'Nom du type de page')
            ->setFormTypeOption('constraints', [
                new Callback(function ($value, ExecutionContextInterface $context) {
                    $entityId = null;
                    $nameExist = false;

                    $entityId = $context->getRoot()->getData()->getId();

                    $nameExist = $this->easyAdminPageTypeService->isPageTypeExist($value, $entityId);

                    if (null !== $value) {
                        if (true === $nameExist) {
                            $context->buildViolation('Le nom de type de page existe déjà.')
                                ->addViolation()
                            ;
                        }
                    }
                }),
            ])
        ;
        yield AssociationField::new('pages', 'Pages liées')
            ->hideOnForm()
            ->setTemplatePath('easyAdmin/field/cta-list.html.twig')
        ;
        yield TextEditorField::new('devkey', 'DevKey')->setFormTypeOption('disabled', true);
        yield DateTimeField::new('createdAt', 'Créé le (heure en UTC)')->setFormTypeOption('disabled', true)->hideOnIndex();
        yield DateTimeField::new('updatedAt', 'Mis à jour le (heure en UTC)')->setFormTypeOption('disabled', true)->hideOnIndex();
        yield DateTimeField::new('deletedAt', 'Supprimé le (heure en UTC)')->setFormTypeOption('disabled', true)->hideOnIndex();
    }

    public function persistEntity(EntityManagerInterface $entityManager, mixed $entityInstance): void
    {
        $entityInstance->setCreatedAt(new \DateTimeImmutable());
        $entityInstance->setUpdatedAt(new \DateTimeImmutable());
        parent::persistEntity($entityManager, $entityInstance);
    }

    /* Set default values on the entity creation */
    public function createEntity(string $entityFqcn): PageType
    {
        $pageType = new PageType();

        $devkey = $this->pageTypeRepository->getLastDevKey() + 1;

        if (null !== $devkey) {
            if (false === $this->pageTypeService->isDevKeyUsed($devkey)) {
                $pageType->setDevKey($devkey);
            } else {
                throw new \Exception('DevKey already used');
            }
        }

        return $pageType;
    }

    public function updateEntity(EntityManagerInterface $entityManager, mixed $entityInstance): void
    {
        /* SETTERS */

        /* We set the dev key if it's not set */
        if (null === $entityInstance->getDevKey()) {
            $devkey = $this->pageTypeRepository->getLastDevKey() + 1;

            if (false === $this->pageTypeService->isDevKeyUsed($devkey)) {
                $entityInstance->setDevKey($devkey);
            } else {
                throw new \Exception('DevKey already used');
            }
        }

        $entityInstance->setUpdatedAt(new \DateTimeImmutable());
        parent::updateEntity($entityManager, $entityInstance);
    }
}
