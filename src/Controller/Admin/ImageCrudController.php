<?php

/*
 * (c) No name
 */

namespace App\Controller\Admin;

use App\Entity\Image;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ImageCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Image::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_EDIT, Action::INDEX)
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_EDIT, Action::DETAIL)
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        yield IdField::new('id')->hideOnForm();
        yield TextField::new('title', 'Titre de l\'image');
        yield TextField::new('name', 'Nom en Français')->hideOnIndex();
        yield TextField::new('nameEN', 'Nom en Anglais')->hideOnIndex();
        yield TextField::new('nameNL', 'Nom en Néerlandais')->hideOnIndex();
        yield ChoiceField::new('category', 'Catégorie')
            ->autocomplete()
            ->setRequired(true)
            ->setChoices([
                'Image' => 'image',
                'Image gallerie' => 'gallery',
                'Bannière' => 'banner',
                'Image miniature' => 'thumbnail',
            ])
        ;
        yield TextField::new('imageFile')->setFormType(VichImageType::class)->hideOnIndex();
        yield ImageField::new('directory', 'Image')->setBasePath('assets/images/medias/')->onlyOnIndex();

        yield TextField::new('directory', 'Nom du fichier')->setFormTypeOption('disabled', true)->hideWhenCreating()->setTemplatePath('easyAdmin/field/image-relativepath.html.twig');

        /* yield NumberField::new('height', 'Hauteur')->hideOnForm();
        yield NumberField::new('width', 'Largeur')->hideOnForm(); */
        yield NumberField::new('size', 'Poid en (mo)')->hideOnIndex()->setFormTypeOption('disabled', true);
        yield TextField::new('alt', 'Texte alternatif');
        yield TextField::new('altEN', 'Texte alternatif en Anglais');
        yield TextField::new('altNL', 'Texte alternatif en Néerlandais');
    }

    public function persistEntity(EntityManagerInterface $entityManager, mixed $entityInstance): void
    {
        $entityInstance->setCreatedAt(new \DateTimeImmutable());
        $entityInstance->setUpdatedAt(new \DateTimeImmutable());
        parent::persistEntity($entityManager, $entityInstance);
    }

    public function updateEntity(EntityManagerInterface $entityManager, mixed $entityInstance): void
    {
        $entityInstance->setUpdatedAt(new \DateTimeImmutable());
        parent::updateEntity($entityManager, $entityInstance);
    }

    public function deleteEntity(EntityManagerInterface $entityManager, mixed $entityInstance): void
    {
        $entityInstance->setDeletedAt(new \DateTimeImmutable());
        parent::deleteEntity($entityManager, $entityInstance);
    }
}
