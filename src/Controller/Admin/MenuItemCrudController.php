<?php

/*
 * (c) No name
 */

namespace App\Controller\Admin;

use App\Entity\Menu;
use App\Entity\MenuItem;
use App\Entity\Page;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class MenuItemCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return MenuItem::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_EDIT, Action::INDEX)
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_EDIT, Action::DETAIL)
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        yield IdField::new('id')->hideOnForm();
        yield AssociationField::new('menu', 'Menu associé')
            ->hideOnIndex()
            ->setRequired(false)
            ->setFormTypeOptions([
                'class' => Menu::class,
                'choice_label' => 'name',
            ])
        ;
        yield AssociationField::new('page', 'Page associée')
            ->hideOnIndex()
            ->setFormTypeOptions([
                'class' => Page::class,
                'choice_label' => 'name',
            ])
        ;
        yield TextField::new('name')->setRequired(true);
        yield TextField::new('nameEN')->setRequired(true)->hideOnIndex();
        yield TextField::new('nameNL')->setRequired(true)->hideOnIndex();
        yield AssociationField::new('children', 'Items enfants')
            ->hideOnIndex()
            ->setFormTypeOptions([
                'class' => MenuItem::class,
                'choice_label' => 'name',
            ])
        ;
        yield NumberField::new('weight');
    }

    public function persistEntity(EntityManagerInterface $entityManager, mixed $entityInstance): void
    {
        parent::persistEntity($entityManager, $entityInstance);
    }

    public function deleteEntity(EntityManagerInterface $entityManager, mixed $entityInstance): void
    {
        parent::deleteEntity($entityManager, $entityInstance);
    }
}
