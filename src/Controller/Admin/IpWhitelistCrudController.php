<?php

/*
 * (c) No name
 */

namespace App\Controller\Admin;

use App\Entity\IpWhitelist;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class IpWhitelistCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return IpWhitelist::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
