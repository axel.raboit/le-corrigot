<?php

/*
 * (c) No name
 */

namespace App\Controller\Admin;

use App\Entity\DataEnum;
use App\Entity\Image;
use App\Entity\IpWhitelist;
use App\Entity\Menu;
use App\Entity\MenuItem as EntityMenuItem;
use App\Entity\Page;
use App\Entity\PageGallery;
use App\Entity\PageType;
use App\Entity\UserBackend;
use App\Entity\Website;
use App\Service\WebsiteService;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    private AdminUrlGenerator $adminUrlGenerator;
    private WebsiteService $websiteService;
    private RequestStack $requestStack;

    public function __construct(
        AdminUrlGenerator $adminUrlGenerator,
        WebsiteService $websiteService,
        RequestStack $requestStack
    ) {
        $this->adminUrlGenerator = $adminUrlGenerator;
        $this->websiteService = $websiteService;
        $this->requestStack = $requestStack;
    }

    #[Route(
        path: '/backend/admin',
        name: 'admin',
        priority: 3,
    )]
    public function index(): Response
    {
        // CREATE A NEW ROUTE: symfony console make:admin:crud

        /* return parent::index(); */

        return $this->redirect($this->adminUrlGenerator->setController(PageCrudController::class)->generateUrl());

        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
        // $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        // return $this->redirect($adminUrlGenerator->setController(OneOfYourCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        // return $this->render('some/path/my-dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        $request = $this->requestStack->getCurrentRequest();
        $hostname = $request->getHost();
        $website = $this->websiteService->getCurrentWebsite($hostname, true);
        $websiteName = $website['name'];

        return Dashboard::new()
            ->setTitle($websiteName)
        ;
    }

    public function configureMenuItems(): iterable
    {
        /* yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home'); */
        /* yield MenuItem::linkToCrud('Page', 'fa fa-home', Page::class); */
        yield MenuItem::subMenu('Pages')->setSubItems([
            MenuItem::linkToCrud('Liste des pages', 'fa fa-list', Page::class)->setAction(Crud::PAGE_INDEX),
            MenuItem::linkToCrud('Ajouter une page', 'fa fa-plus', Page::class)->setAction(Crud::PAGE_NEW),
        ]);
        yield MenuItem::subMenu('Type de page')->setSubItems([
            MenuItem::linkToCrud('Liste des types de page', 'fa fa-list', PageType::class)->setAction(Crud::PAGE_INDEX),
            MenuItem::linkToCrud('Ajouter un type de page', 'fa fa-plus', PageType::class)->setAction(Crud::PAGE_NEW),
        ]);
        yield MenuItem::subMenu('Menus')->setSubItems([
            MenuItem::linkToCrud('Liste des menus', 'fa fa-list', Menu::class)->setAction(Crud::PAGE_INDEX),
            MenuItem::linkToCrud('Ajouter un menu', 'fa fa-plus', Menu::class)->setAction(Crud::PAGE_NEW),
        ]);
        yield MenuItem::subMenu('Elements de menu')->setSubItems([
            MenuItem::linkToCrud('Liste des elements de menu', 'fa fa-list', EntityMenuItem::class)->setAction(Crud::PAGE_INDEX),
            MenuItem::linkToCrud('Ajouter un element de menu', 'fa fa-plus', EntityMenuItem::class)->setAction(Crud::PAGE_NEW),
        ]);
        yield MenuItem::subMenu('Galleries')->setSubItems([
            MenuItem::linkToCrud('Liste des associations', 'fa fa-list', PageGallery::class)->setAction(Crud::PAGE_INDEX),
            MenuItem::linkToCrud('Associé une image à une page', 'fa fa-plus', PageGallery::class)->setAction(Crud::PAGE_NEW),
        ]);
        yield MenuItem::subMenu('Images')->setSubItems([
            MenuItem::linkToCrud('Liste des images', 'fa fa-list', Image::class)->setAction(Crud::PAGE_INDEX),
            MenuItem::linkToCrud('Ajouter une image', 'fa fa-plus', Image::class)->setAction(Crud::PAGE_NEW),
        ]);
        yield MenuItem::subMenu('Utilisateurs')->setSubItems([
            MenuItem::linkToCrud('Liste des Utilisateurs', 'fa fa-list', UserBackend::class)->setAction(Crud::PAGE_INDEX),
            MenuItem::linkToCrud('Ajouter un utilisateur', 'fa fa-plus', UserBackend::class)->setAction(Crud::PAGE_NEW),
        ]);
        yield MenuItem::subMenu('Site web')->setSubItems([
            MenuItem::linkToCrud('Liste des sites web', 'fa fa-list', Website::class)->setAction(Crud::PAGE_INDEX),
        ]);
        yield MenuItem::subMenu('Dev')->setSubItems([
            MenuItem::linkToCrud('Whitelist', 'fa fa-list', IpWhitelist::class)->setAction(Crud::PAGE_INDEX),
            MenuItem::linkToCrud('Liste des DataEnum', 'fa fa-list', DataEnum::class)->setAction(Crud::PAGE_INDEX),
        ]);
    }
}
