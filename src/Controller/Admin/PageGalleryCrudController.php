<?php

/*
 * (c) No name
 */

namespace App\Controller\Admin;

use App\Entity\Image;
use App\Entity\Page;
use App\Entity\PageGallery;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;

/* use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField; */

class PageGalleryCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return PageGallery::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_EDIT, Action::INDEX)
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_EDIT, Action::DETAIL)
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        yield IdField::new('id')->hideOnForm();
        /* yield TextField::new('title')->hideOnIndex();
        yield TextField::new('subtitle')->hideOnIndex();
        yield TextEditorField::new('description')->hideOnIndex();
        yield TextField::new('imageAlt')->onlyWhenUpdating();
        yield TextField::new('imageUrl')->hideOnIndex();
        yield TextField::new('imageTitle')->hideOnIndex();
        yield TextField::new('ctaText')->hideOnIndex();
        yield TextField::new('ctaTitle')->hideOnIndex();
        yield TextField::new('ctaUrl')->hideOnIndex();
        yield IntegerField::new('weight')->hideOnIndex(); */
        yield AssociationField::new('page', 'Page')
            ->setFormTypeOptions([
                'class' => Page::class,
                'choice_label' => 'title',
            ])
        ;
        yield AssociationField::new('image', 'Image (Associée à la page)')
            ->setFormTypeOptions([
                'class' => Image::class,
                'choice_label' => 'title',
            ])
        ;
        yield DateTimeField::new('createdAt')->setFormTypeOption('disabled', true)->hideOnIndex();
        yield DateTimeField::new('updatedAt')->setFormTypeOption('disabled', true)->hideOnIndex();
        yield DateTimeField::new('deletedAt')->setFormTypeOption('disabled', true)->hideOnIndex();
    }

    public function persistEntity(EntityManagerInterface $entityManager, mixed $entityInstance): void
    {
        $entityInstance->setCreatedAt(new \DateTimeImmutable());
        $entityInstance->setUpdatedAt(new \DateTimeImmutable());
        parent::persistEntity($entityManager, $entityInstance);
    }

    public function deleteEntity(EntityManagerInterface $entityManager, mixed $entityInstance): void
    {
        $entityInstance->setDeletedAt(new \DateTimeImmutable());
        parent::deleteEntity($entityManager, $entityInstance);
    }
}
