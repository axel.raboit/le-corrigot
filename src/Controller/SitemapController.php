<?php

declare(strict_types=1);

/*
 * (c) No name
 */

namespace App\Controller;

use App\Repository\PageRepository;
use App\Service\DateService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class SitemapController extends AbstractController
{
    private DateService $dateService;
    private PageRepository $pageRepository;

    public function __construct(PageRepository $pageRepository, DateService $dateService)
    {
        $this->dateService = $dateService;
        $this->pageRepository = $pageRepository;
    }

    /**
     * @Route("/sitemap.xml", name="sitemap")
     */
    #[Route(
        path: '/sitemap.xml',
        name: 'sitemap',
        defaults: [
            '_format' => 'xml',
        ],
    )]
    public function index(Request $request): Response
    {
        $hostname = $request->getSchemeAndHttpHost();

        $urls = [];
        /* foreach ($pages as $page) {
            $urls[] = [
                'loc' => $this->generateUrl(
                    'page',
                    ['slug' => $page->getSlug()],
                    UrlGeneratorInterface::ABSOLUTE_URL
                ),
                'lastmod' => $page->getUpdatedAt()->format('Y-m-d'),
                'changefreq' => 'weekly',
                'priority' => '0.5',
            ];
        } */

        // We add the dynamic URLs of the articles in the table
        foreach ($this->pageRepository->findAll() as $page) {
            $formatedDate = null;
            if (null !== $page->getUpdatedAt()) {
                $date = $page->getUpdatedAt();
                $formatedDate = $this->dateService->dateToISO8601($date);
            }

            if (null !== $page->getImageThumbnail()) {
                $images = [
                    'loc' => $page->getImageThumbnail()->getDirectory(), // URL to image
                    'title' => $page->getName(),    // Optional, text describing the image
                ];

                if (null !== $page->getUpdatedAt() && null !== $formatedDate) {
                    $urls[] = [
                        'loc' => $this->generateUrl('page_index', [
                            'slug' => $page->getSlug(),
                        ]),
                        'lastmod' => $formatedDate,
                        'image' => $images,
                    ];
                } else {
                    $urls[] = [
                        'loc' => $this->generateUrl('page_index', [
                            'slug' => $page->getSlug(),
                        ]),
                        'image' => $images,
                    ];
                }
            } else {
                if (null !== $page->getUpdatedAt() && null !== $formatedDate) {
                    $urls[] = [
                        'loc' => $this->generateUrl('page_index', [
                            'slug' => $page->getSlug(),
                        ]),
                        'lastmod' => $formatedDate,
                    ];
                } else {
                    $urls[] = [
                        'loc' => $this->generateUrl('page_index', [
                            'slug' => $page->getSlug(),
                        ]),
                    ];
                }
            }
        }

        /* Remove every urls that contains /backend/ */
        foreach ($urls as $key => $url) {
            if (str_contains($url['loc'], '/backend/')) {
                unset($urls[$key]);
            }
        }

        $response = new Response(
            $this->renderView('extra/sitemap.html.twig', [
                'urls' => $urls,
                'hostname' => $hostname,
            ]),
            200
        );

        $response->headers->set('Content-Type', 'text/xml');

        return $response;
    }
}
