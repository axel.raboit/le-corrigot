<?php

declare(strict_types=1);

/*
 * (c) No name
 */

namespace App\Controller;

use App\Repository\PageRepository;
use App\Service\PageGalleryService;
use App\Service\PageService;
use App\Service\RouteService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PageController extends AbstractController
{
    public const PAGE_DEFAULT = 'page_index';

    private PageService $pageService;
    private PageRepository $pageRepository;
    private PageGalleryService $pageGalleryService;
    private RouteService $routeService;

    public function __construct(
        PageService $pageService,
        PageRepository $pageRepository,
        PageGalleryService $pageGalleryService,
        RouteService $routeService,
    ) {
        $this->pageService = $pageService;
        $this->pageRepository = $pageRepository;
        $this->pageGalleryService = $pageGalleryService;
        $this->routeService = $routeService;
    }

    #[Route(
        path: '{slug}',
        name: self::PAGE_DEFAULT,
        requirements: [
            'slug' => '.+',
        ],
        priority: 1,
    )]
    public function index(string $slug, Request $request): Response|RedirectResponse
    {
        if (false === $this->isGranted('CAN_ACCESS_WHEN_MAINTENANCE_IS_ACTIVATED')) {
            return $this->redirectToRoute('maintenance');
        }

        /* THIS CODE IS TEMPORARY */
        /* $url = $request->getSchemeAndHttpHost().$request->getRequestUri();
        if (false !== mb_strpos($url, 'index.php')) {
            return $this->redirect($request->getSchemeAndHttpHost().str_replace('index.php', '', $request->getRequestUri()));
        } */
        /* THIS CODE IS TEMPORARY */

        $page = $this->pageRepository->getPageBySlug($slug);

        $routes = $this->routeService->getRoutes();
        $route = $this->redirectOnRoute($request, $routes);

        if (null !== $page) {
            if ($page->getDevCodeRouteName()) {
                $route = $routes[$page->getDevCodeRouteName()];
                if (!mb_strstr($route->getPath(), '{slug}') && !mb_strstr($route->getPath(), '{id}')) {
                    return $this->forward($route->getDefaults()['_controller'], ['page' => $page]);
                }

                $params['slug'] = $page->getSlug();
                $params['page'] = $page;

                return $this->forward($route->getDefaults()['_controller'], $params);
            }

            $elements = $this->pageService->getPageElements($page, $request);

            $elements['children'] = $this->pageService->getChildrenFromPage($page, $request);
            $elements['gallery'] = $this->pageGalleryService->getPageGalleryElements($page, $request);

            return $this->render($elements['template'], $elements);
        }

        if (null !== $route) {
            return $this->forward($route->getDefaults()['_controller']);
        }

        /* If no page has been found display a 404 (This, don't need condition because it's the last declaration) */
        $page = $this->pageService->page404NotFound();
        $elements = $this->pageService->getPageElements($page, $request);
        $response = $this->render($elements['template'], $elements);
        $response->setStatusCode(Response::HTTP_NOT_FOUND);

        return $response;
    }

    /**
     * @return mixed
     */
    private function redirectOnRoute(Request $request, array $routes)
    {
        if (mb_strstr($request->getRequestUri(), 'sitemap')) {
            return $routes['sitemap'];
        }

        if (mb_strstr($request->getRequestUri(), 'admin')) {
            return $routes['admin'];
        }
    }
}
