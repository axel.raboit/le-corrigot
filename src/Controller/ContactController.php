<?php

declare(strict_types=1);

/*
 * (c) No name
 */

namespace App\Controller;

use App\Entity\Contact;
use App\Entity\Page;
use App\Enum\DataEnum;
use App\Form\ContactType;
use App\Manager\DataEnumManager;
use App\Repository\PageRepository;
use App\Service\ContactService;
use App\Service\GoogleCaptchaService;
use App\Service\MailerService;
use App\Service\PageService;
use App\Service\WebsiteService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class ContactController extends AbstractController
{
    public const RECAPTCHA_SCORE_LIMIT = 0.5;

    private PageService $pageService;
    private PageRepository $pageRepository;
    private TranslatorInterface $translator;
    private WebsiteService $websiteService;
    private DataEnumManager $dataEnumManager;
    private MailerService $mailerService;
    private GoogleCaptchaService $googleCaptchaService;
    private ContactService $contactService;

    public function __construct(
        PageService $pageService,
        PageRepository $pageRepository,
        TranslatorInterface $translator,
        GoogleCaptchaService $googleCaptchaService,
        DataEnumManager $dataEnumManager,
        MailerService $mailerService,
        WebsiteService $websiteService,
        ContactService $contactService,
    ) {
        $this->pageService = $pageService;
        $this->pageRepository = $pageRepository;
        $this->translator = $translator;
        $this->googleCaptchaService = $googleCaptchaService;
        $this->websiteService = $websiteService;
        $this->dataEnumManager = $dataEnumManager;
        $this->mailerService = $mailerService;
        $this->contactService = $contactService;
    }

    #[Route(
        path: '/contact/politique-de-confidentialite',
        name: 'contact_privacy_policy',
    )]
    public function privacyPolicy(Request $request): Response
    {
        $page = $this->pageRepository->getPageFromDataDevKey(DataEnum::DATA_PAGE_CONTACT_PRIVACY_POLICY_DEV_KEY);

        $elements = $this->pageService->getPageElements($page, $request);

        return $this->render($elements['template'], $elements);
    }

    #[Route(
        path: '/contact',
        name: 'contact',
    )]
    public function contact(Request $request, Page $page): Response
    {
        /* If you want to get the page by the externaly way */
        /* $page = $this->pageRepository->getPageFromDataDevKey(DataEnum::DATA_PAGE_CONTACT_DEV_KEY); */
        /* Make sure to declare the DataEnum, and link the dataEnum to the page */

        // Get recaptcha public key & secret key
        $recapatchaPublicKey = $this->dataEnumManager->getDataEnumValue(DataEnum::DATA_RECAPTCHA_PUBLIC_DEV_KEY);
        $recapatchaSecretKey = $this->dataEnumManager->getDataEnumValue(DataEnum::DATA_RECAPTCHA_SECRET_DEV_KEY);

        // Get the email address of the website owner
        $website = $this->websiteService->getCurrentWebsite($request->getHost(), true);
        $websiteEmailOwner = $website['email'];

        $elements = $this->pageService->getPageElements($page, $request);

        /* Warning: This Contact object is created but desactivated, so no data will be stocked inside the database.
        But the feature is here anyway, we can activate it in mailerService::contactMailSend */
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact, [
            'lang' => $request->getLocale(),
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $isMailSent = false;

                /** @var string $captcha */
                $captcha = $form->get('recaptcha')->getData();

                if (empty($captcha && null !== $recapatchaPublicKey && null !== $recapatchaSecretKey)) {
                    return $this->redirectToRoute($request->attributes->get('_route'));
                }

                $dataContactObject = $form->getData();

                $data = $this->contactService->prepareDataForEmail($dataContactObject, $request);

                if (null !== $recapatchaPublicKey && '' !== $recapatchaPublicKey
                && null !== $recapatchaSecretKey && '' !== $recapatchaSecretKey
                ) {
                    $response = $this->googleCaptchaService->verify($captcha);
                    $recaptchaScore = $response['score'];

                    $data = $this->contactService->prepareDataForEmail($dataContactObject, $request, $recaptchaScore);

                    if (true === $response['success'] && $recaptchaScore >= self::RECAPTCHA_SCORE_LIMIT) {
                        $isMailSent = $this->mailerService->contactMailSend($dataContactObject, $data, $websiteEmailOwner);
                    }
                } else {
                    $isMailSent = $this->mailerService->contactMailSend($dataContactObject, $data, $websiteEmailOwner);
                }

                if (true === $isMailSent) {
                    $this->addFlash('success', $this->translator->trans('form.contact.message.success'));

                    return $this->redirectToRoute('contact');
                }

                $this->addFlash('danger', $this->translator->trans('form.contact.message.error'));
            } else {
                $this->addFlash('danger', $this->translator->trans('form.contact.message.error'));
            }
        }

        $elements['form'] = $form->createView();

        return $this->render($elements['template'], $elements);
    }
}
