<?php

declare(strict_types=1);

/*
 * (c) No name
 */

namespace App\Service;

use App\Entity\Contact;
use Symfony\Component\HttpFoundation\Request;

class ContactService
{
    public function prepareDataForEmail(Contact $dataContactObject, Request $request, float $recaptchaScore = null): array
    {
        /** @var array $data */
        $data = [];

        $data['clientIp'] = $request->getClientIp();
        $data['userAgent'] = $_SERVER['HTTP_USER_AGENT'];
        $data['name'] = $dataContactObject->getName();
        $data['email'] = $dataContactObject->getEmail();
        $data['subject'] = $dataContactObject->getSubject();
        $data['message'] = $dataContactObject->getMessage();
        $data['address'] = $dataContactObject->getAddress();
        $data['postalCode'] = $dataContactObject->getPostalCode();
        $data['city'] = $dataContactObject->getCity();
        $data['country'] = $dataContactObject->getCountry();
        $data['phone'] = $dataContactObject->getPhone();

        if (null !== $recaptchaScore) {
            $data['score'] = $recaptchaScore;
        }

        return $data;
    }
}
