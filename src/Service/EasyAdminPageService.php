<?php

declare(strict_types=1);

/*
 * (c) No name
 */

namespace App\Service;

use App\Repository\PageRepository;

class EasyAdminPageService
{
    /** @var PageRepository */
    private $pageRepository;

    public function __construct(
        PageRepository $pageRepository,
    ) {
        $this->pageRepository = $pageRepository;
    }

    public function isSlugExist(string $slug, int $entityId = null): bool
    {
        $page = $this->pageRepository->findOneBy(['slug' => $slug]);

        /* Case: if the entity is already created and the page found is the same as the current entity */
        if (null !== $page) {
            if (null !== $entityId && $entityId === $page->getId()) {
                return false;
            }
        }

        /* Case: if the entity is not created yet and the page found has the same slug */
        if (null === $entityId && null !== $page) {
            return true;
        }

        if (null === $page) {
            return false;
        }

        return true;
    }

    public function isCanonicalUrlExist(string $canonicalUrl = null, int $entityId = null): bool
    {
        if (null === $canonicalUrl) {
            return false;
        }

        $page = $this->pageRepository->findOneBy(['canonicalUrl' => $canonicalUrl]);

        /* Case: if the entity is already created and the page found is the same as the current entity */
        if (null !== $page) {
            if (null !== $entityId && $entityId === $page->getId()) {
                return false;
            }
        }

        /* Case: if the entity is not created yet and the page found has the same slug */
        if (null === $entityId && null !== $page) {
            return true;
        }

        if (null === $page) {
            return false;
        }

        return true;
    }
}
