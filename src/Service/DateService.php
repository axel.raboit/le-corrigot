<?php

declare(strict_types=1);

/*
 * (c) No name
 */

namespace App\Service;

class DateService
{
    public function dateToISO8601(\DateTimeInterface $date): string
    {
        $dateStringified = $date->format('Y-m-d\TH:i:sP');
        $dateTimestamp = strtotime($dateStringified);

        return date('Y-m-d\TH:i:sO', $dateTimestamp);
    }
}
