<?php

declare(strict_types=1);

/*
 * (c) No name
 */

namespace App\Service;

use App\Repository\PageTypeRepository;

class EasyAdminPageTypeService
{
    /** @var PageTypeRepository */
    private $pageTypeRepository;

    public function __construct(
        PageTypeRepository $pageTypeRepository,
    ) {
        $this->pageTypeRepository = $pageTypeRepository;
    }

    public function isPageTypeExist(string $pageTypeName = null, int $entityId = null): bool
    {
        if (null === $pageTypeName) {
            return false;
        }

        $page = $this->pageTypeRepository->findOneBy(['name' => $pageTypeName]);

        /* Case: if the entity is already created and the page found is the same as the current entity */
        if (null !== $page) {
            if (null !== $entityId && $entityId === $page->getId()) {
                return false;
            }
        }

        /* Case: if the entity is not created yet and the page found has the same page type name */
        if (null === $entityId && null !== $page) {
            return true;
        }

        if (null === $page) {
            return false;
        }

        return true;
    }
}
