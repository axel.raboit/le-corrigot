<?php

declare(strict_types=1);

/*
 * (c) No name
 */

namespace App\Service;

use App\Entity\Page;
use App\Repository\PageGalleryRepository;
use Symfony\Component\HttpFoundation\Request;

class PageGalleryService
{
    /** @var PageGalleryRepository */
    private $pageGalleryRepository;

    public function __construct(
        PageGalleryRepository $pageGalleryRepository
    ) {
        $this->pageGalleryRepository = $pageGalleryRepository;
    }

    public function getPageGalleryElements(Page $page, Request $request): array
    {
        $language = $request->getLocale();

        $gallery = $this->pageGalleryRepository->getPageGallery($page);

        return $this->getPageGalleryElementsTranslation($gallery, $language);
    }

    private function getPageGalleryElementsTranslation(array $gallery, string $language): array
    {
        $pageElements = [];

        foreach ($gallery as $key => $item) {
            $image = $item['image'];
            $itemElement = [];

            if ('en' === $language) {
                if (null !== $image) {
                    if (null !== $image->getNameEn()) {
                        $itemElement['imageName'] = $image->getNameEn();
                    }

                    if (null !== $image->getAltEn()) {
                        $itemElement['imageAlt'] = $image->getAltEn();
                    }
                }
            } elseif ('fr' === $language) {
                if (null !== $image) {
                    if (null !== $image->getName()) {
                        $itemElement['imageName'] = $image->getName();
                    }

                    if (null !== $image->getAlt()) {
                        $itemElement['imageAlt'] = $image->getAlt();
                    }
                }
            } elseif ('nl' === $language) {
                if (null !== $image) {
                    if (null !== $image->getNameNl()) {
                        $itemElement['imageName'] = $image->getNameNl();
                    }

                    if (null !== $image->getAltNl()) {
                        $itemElement['imageAlt'] = $image->getAltNl();
                    }
                }
            }

            $pageElements[$key] = [
                'elements' => [
                    'gallery' => $item,
                    'imageElements' => $itemElement,
                ],
            ];
        }

        return $pageElements;
    }
}
