<?php

declare(strict_types=1);

/*
 * (c) No name
 */

namespace App\Service;

use App\Entity\Website;
use App\Repository\WebsiteRepository;
use Symfony\Component\HttpKernel\KernelInterface;

class WebsiteService
{
    public const DEV_HOSTNAME_NUMBERS = '127.0.0.1';
    public const DEV_HOSTNAME_LOCALHOST = 'localhost';

    private WebsiteRepository $websiteRepository;
    private KernelInterface $kernel;

    public function __construct(
        WebsiteRepository $websiteRepository,
        KernelInterface $kernel
    ) {
        $this->websiteRepository = $websiteRepository;
        $this->kernel = $kernel;
    }

    /**
     * @return Website|array
     */
    public function getCurrentWebsite(string $hostname, bool $isArray = false)
    {
        /* If it's the dev env */
        if (self::DEV_HOSTNAME_NUMBERS === $hostname || self::DEV_HOSTNAME_LOCALHOST === $hostname) {
            $website = $this->websiteRepository->findOneBy(['hostname' => self::DEV_HOSTNAME_NUMBERS]);

            if (!$website) {
                $website = $this->websiteRepository->findOneBy(['hostname' => self::DEV_HOSTNAME_LOCALHOST]);
            }

            if (!$website) {
                throw new \Exception('Website not found, or make sure to provide a valid hostname for dev environment');
            }
            /* If it's a production env (staging, preprod, prod) */
        } else {
            if (false === mb_strpos($hostname, 'www.')) {
                if ('prod' === $this->kernel->getEnvironment()) {
                    $hostname = 'www.'.$hostname;
                }
            }

            $website = $this->websiteRepository->findOneBy(['hostname' => $hostname]);

            if (!$website) {
                throw new \Exception('Website not found Provided hostnaname: '.$hostname);
            }
        }

        if ($isArray) {
            $website = $website->toArray();
        }

        return $website;
    }

    public function getWebsite(): Website
    {
        $website = $this->websiteRepository->findOneBy([]);

        if (!$website) {
            throw new \Exception('Website not found');
        }

        return $website;
    }
}
