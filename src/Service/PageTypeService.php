<?php

declare(strict_types=1);

/*
 * (c) No name
 */

namespace App\Service;

use App\Repository\PageTypeRepository;

class PageTypeService
{
    /** @var PageTypeRepository */
    private $pageTypeRepository;

    public function __construct(
        PageTypeRepository $pageTypeRepository,
    ) {
        $this->pageTypeRepository = $pageTypeRepository;
    }

    /* CHECKERS */

    public function isDevKeyUsed(int $devKey): bool
    {
        $pageType = $this->pageTypeRepository->findOneBy(['devKey' => $devKey]);

        if (null === $pageType) {
            return false;
        }

        return true;
    }
}
