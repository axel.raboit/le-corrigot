<?php

declare(strict_types=1);

/*
 * (c) No name
 */

namespace App\Service;

use App\Entity\Page;
use App\Enum\DataEnum;
use App\Repository\PageRepository;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class PageService
{
    /** @var PageRepository */
    private $pageRepository;

    /** @var Request */
    private $request;

    public function __construct(
        PageRepository $pageRepository,
        RequestStack $requestStack,
    ) {
        $this->pageRepository = $pageRepository;
        $this->request = $requestStack->getCurrentRequest();
    }

    public function page404NotFound(): Page
    {
        return $this->pageRepository->getPageFromDataDevKey(DataEnum::DATA_PAGE_ERROR_404_DEV_KEY);
    }

    public function getPageByDevCodeRouteName(string $devCodeRouteName): Page
    {
        return $this->pageRepository->findOneBy(['devCodeRouteName' => $devCodeRouteName]);
    }

    public function getPageElements(Page $page, Request $request): array
    {
        $template = $page->getTemplate();
        $language = $request->getLocale();

        $pageElements = $this->getPageElementsTranslation($page, $language);

        $pageElementsFormated = [];
        foreach ($pageElements as $key => $element) {
            if (null !== $element && '' !== $element && !is_numeric($element)) {
                $pageElementsFormated[$key] = $this->parseRelativeUrlWithLocale($element);
            } else {
                $pageElementsFormated[$key] = $element;
            }
        }

        /* This is all what $elements in for example PageController::index returns */
        /* $elements = $this->pageService->getPageElements($page, $request); */
        return [
            /* If we want to use "slug", "template", "devKey", etc... then we should use page */
            'page' => $page,
            /* If we want to use translated things like for example "titleEN", then we should use pageElements */
            /* The content of pageElements is translated to call only like "title", this will be automaticaly
            translated from titleEN to title and being in English or others language */
            'pageElements' => $pageElementsFormated,
            /* That's the current locale (language) */
            'language' => $language,
            /* That's the template we use about the page (also provided inside page variable) */
            'template' => $template,
        ];
    }

    private function getPageElementsTranslation(Page $page, string $language): array
    {
        $pageElements = [];

        if ('en' === $language) {
            $pageElements['bannerTitle'] = $page->getBannerTitleEn();
            if (null !== $page->getBanner()) {
                $pageElements['bannerName'] = $page->getBanner()->getNameEn();
            }
            if (null !== $page->getBanner()) {
                $pageElements['bannerAlt'] = $page->getBanner()->getAltEn();
            }
            $pageElements['metaDescription'] = $page->getMetaDescriptionEN();
            $pageElements['ctaTitle'] = $page->getCtaTitleEn();
            $pageElements['name'] = $page->getNameEn();
            $pageElements['title'] = $page->getTitleEn();
            $pageElements['preDescription'] = $page->getPreDescriptionEn();
            $pageElements['description'] = $page->getDescriptionEn();
            $pageElements['contentPrimary'] = $page->getContentPrimaryEn();
            $pageElements['contentSecondary'] = $page->getContentSecondaryEn();
            $pageElements['contentTertiary'] = $page->getContentTertiaryEn();
            $pageElements['contentQuaternary'] = $page->getContentQuaternaryEn();
        } elseif ('fr' === $language) {
            $pageElements['bannerTitle'] = $page->getBannerTitle();
            if (null !== $page->getBanner()) {
                $pageElements['bannerName'] = $page->getBanner()->getName();
            }
            if (null !== $page->getBanner()) {
                $pageElements['bannerAlt'] = $page->getBanner()->getAlt();
            }
            $pageElements['metaDescription'] = $page->getMetaDescription();
            $pageElements['ctaTitle'] = $page->getCtaTitle();
            $pageElements['name'] = $page->getName();
            $pageElements['title'] = $page->getTitle();
            $pageElements['preDescription'] = $page->getPreDescription();
            $pageElements['description'] = $page->getDescription();
            $pageElements['contentPrimary'] = $page->getContentPrimary();
            $pageElements['contentSecondary'] = $page->getContentSecondary();
            $pageElements['contentTertiary'] = $page->getContentTertiary();
            $pageElements['contentQuaternary'] = $page->getContentQuaternary();
        } elseif ('nl' === $language) {
            $pageElements['bannerTitle'] = $page->getBannerTitleNl();
            if (null !== $page->getBanner()) {
                $pageElements['bannerName'] = $page->getBanner()->getNameNl();
            }
            if (null !== $page->getBanner()) {
                $pageElements['bannerAlt'] = $page->getBanner()->getAltNl();
            }
            $pageElements['metaDescription'] = $page->getMetaDescriptionNL();
            $pageElements['ctaTitle'] = $page->getCtaTitleNl();
            $pageElements['name'] = $page->getNameNl();
            $pageElements['title'] = $page->getTitleNl();
            $pageElements['preDescription'] = $page->getPreDescriptionNl();
            $pageElements['description'] = $page->getDescriptionNl();
            $pageElements['contentPrimary'] = $page->getContentPrimaryNl();
            $pageElements['contentSecondary'] = $page->getContentSecondaryNl();
            $pageElements['contentTertiary'] = $page->getContentTertiaryNl();
            $pageElements['contentQuaternary'] = $page->getContentQuaternaryNl();
        }

        return $pageElements;
    }

    private function parseRelativeUrlWithLocale(string $content): string
    {
        $locale = $this->request->getLocale();

        $crawler = new Crawler($content);

        $relativeLinks = $crawler->filterXPath('//a[starts-with(@href, "/")]')->each(function (Crawler $node) {
            return [
                '_text' => $node->text(),
                'href' => $node->attr('href'),
                'class' => $node->attr('class'),
                'element' => $node->outerHtml(),
            ];
        });

        if (!empty($relativeLinks)) {
            foreach ($relativeLinks as $urlParts) {
                $relativeUrl = $urlParts['href'];
                $classes = $urlParts['class'];
                $oldElement = $urlParts['element'];

                preg_match('#^/([a-z]{2})/#', $relativeUrl, $matches);

                if (empty($matches)) {
                    if ('' !== $classes) {
                        $newElement = '<a href="/'.$locale.$relativeUrl.'" class="'.$classes.'">'.$urlParts['_text'].'</a>';
                    } else {
                        $newElement = '<a href="/'.$locale.$relativeUrl.'">'.$urlParts['_text'].'</a>';
                    }

                    $content = str_replace($oldElement, $newElement, $content);
                }
            }
        }

        return $content;
    }

    public function getChildrenFromPage(Page $page, Request $request): array
    {
        $children = $this->pageRepository->getChildren($page);

        $childrenFormatted = [];

        foreach ($children as $child) {
            $childrenFormatted[] = $this->getPageElements($child, $request);
        }

        return $childrenFormatted;
    }

    /* CHECKERS */

    public function isSlugExist(string $slug): bool
    {
        $page = $this->pageRepository->findOneBy(['slug' => $slug]);

        if (null === $page) {
            return false;
        }

        return true;
    }

    public function isDevKeyUsed(int $devKey): bool
    {
        $page = $this->pageRepository->findOneBy(['devKey' => $devKey]);

        if (null === $page) {
            return false;
        }

        return true;
    }
}
