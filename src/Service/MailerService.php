<?php

declare(strict_types=1);

/*
 * (c) No name
 */

namespace App\Service;

use App\Entity\Contact;
use App\Enum\DataEnum;
use App\Manager\ContactManager;
use App\Manager\DataEnumManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Twig\Environment;

class MailerService
{
    /* private ContactManager $contactManager; */
    /* private EntityManagerInterface $manager; */
    private Environment $twig;
    private DataEnumManager $dataEnumManager;
    private MailerInterface $mailer;

    public function __construct(
        /* ContactManager $contactManager, */
        /* EntityManagerInterface $manager, */
        Environment $twig,
        DataEnumManager $dataEnumManager,
        MailerInterface $mailer,
    ) {
        /* $this->contactManager = $contactManager; */
        /* $this->manager = $manager; */
        $this->twig = $twig;
        $this->dataEnumManager = $dataEnumManager;
        $this->mailer = $mailer;
    }

    public function contactMailSend(Contact $dataContactObject, array $data, string $websiteEmailOwner): bool
    {
        /* We can save the contact's mail by activating this following line */
        /* $this->contactManager->save($dataContactObject); */

        $mailerEmail = (new Email());
        $mailerEmail
            ->from($data['email'])
            ->to($websiteEmailOwner)
            ->subject($data['subject'])
            ->html($this->twig->render($this->dataEnumManager->getDataEnumValue(DataEnum::DATA_TEMPLATE_EMAIL_CONTACT_DEV_KEY), [
                'contact' => $data,
            ]))
        ;

        $this->mailer->send($mailerEmail);

        return true;
    }
}
