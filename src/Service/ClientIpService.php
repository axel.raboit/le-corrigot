<?php

declare(strict_types=1);

/*
 * (c) No name
 */

namespace App\Service;

use App\Repository\IpWhitelistRepository;

class ClientIpService
{
    private IpWhitelistRepository $ipWhitelistRepository;

    public function __construct(IpWhitelistRepository $ipWhitelistRepository)
    {
        $this->ipWhitelistRepository = $ipWhitelistRepository;
    }

    public function clientIpIsAllowed(): bool
    {
        $clientIp = $_SERVER['REMOTE_ADDR'];

        return $this->ipWhitelistRepository->findIp($clientIp);
    }
}
