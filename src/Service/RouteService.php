<?php

declare(strict_types=1);

/*
 * (c) No name
 */

namespace App\Service;

use Symfony\Component\Routing\RouterInterface;

class RouteService
{
    private RouterInterface $router;

    public function __construct(
        RouterInterface $router,
    ) {
        $this->router = $router;
    }

    public function getRoutes(): array
    {
        $routes = [];
        foreach ($this->router->getRouteCollection()->all() as $routeName => $route) {
            $routes[$routeName] = $route;
        }

        return $routes;
    }
}
