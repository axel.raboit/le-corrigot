<?php

declare(strict_types=1);

/*
 * (c) No name
 */

namespace App\Entity;

use App\Repository\PageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PageRepository::class)]
class Page
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    private ?string $template = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $contentSecondary = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $contentTertiary = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $contentQuaternary = null;

    #[ORM\Column(length: 255)]
    private ?string $nameEN = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $descriptionEN = null;

    #[ORM\Column(length: 255)]
    private ?string $titleEN = null;

    #[ORM\Column(length: 255)]
    private ?string $nameNL = null;

    #[ORM\Column(length: 255)]
    private ?string $titleNL = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $descriptionNL = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $contentPrimaryEN = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $contentPrimaryNL = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $contentSecondaryEN = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $contentSecondaryNL = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $contentTertiaryEN = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $contentTertiaryNL = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $contentQuaternaryEN = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $contentQuaternaryNL = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $devCodeRouteName = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $ctaTitle = null;

    #[ORM\Column(length: 100, nullable: true)]
    private ?string $ctaText = null;

    #[ORM\Column(length: 100, nullable: true)]
    private ?string $ctaTextEN = null;

    #[ORM\Column(length: 100, nullable: true)]
    private ?string $ctaTextNL = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $ctaUrl = null;

    #[ORM\Column]
    private ?int $weight = 0;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'children', fetch: 'EAGER')]
    private ?self $parent = null;

    #[ORM\OneToMany(mappedBy: 'parent', targetEntity: self::class)]
    private Collection $children;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $bannerTitle = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $bannerTitleEN = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $bannerTitleNL = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $ctaTitleEN = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $ctaTitleNL = null;

    #[ORM\ManyToOne(targetEntity: Image::class, fetch: 'EAGER')]
    private ?Image $banner = null;

    #[ORM\ManyToOne(targetEntity: Image::class, fetch: 'EAGER')]
    private ?Image $ImageThumbnail = null;

    #[ORM\Column(length: 255)]
    private ?string $slug = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $createdAt;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $updatedAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $deletedAt = null;

    #[ORM\Column(nullable: true)]
    private ?int $devKey = null;

    #[ORM\OneToMany(mappedBy: 'page', targetEntity: PageGallery::class)]
    private Collection $pageGalleries;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $category = null;

    #[ORM\ManyToOne(inversedBy: 'pages')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Website $website = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $metaDescription = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $metaDescriptionEN = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $metaDescriptionNL = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $canonicalUrl = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $contentPrimary = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $preDescription = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $preDescriptionEN = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $preDescriptionNL = null;

    #[ORM\ManyToOne(inversedBy: 'pages')]
    private ?PageType $type = null;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->pageGalleries = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTemplate(): ?string
    {
        return $this->template;
    }

    public function setTemplate(string $template): self
    {
        $this->template = $template;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContentSecondary(): ?string
    {
        return $this->contentSecondary;
    }

    public function setContentSecondary(?string $contentSecondary): self
    {
        $this->contentSecondary = $contentSecondary;

        return $this;
    }

    public function getContentTertiary(): ?string
    {
        return $this->contentTertiary;
    }

    public function setContentTertiary(?string $contentTertiary): self
    {
        $this->contentTertiary = $contentTertiary;

        return $this;
    }

    public function getContentQuaternary(): ?string
    {
        return $this->contentQuaternary;
    }

    public function setContentQuaternary(?string $contentQuaternary): self
    {
        $this->contentQuaternary = $contentQuaternary;

        return $this;
    }

    public function getNameEN(): ?string
    {
        return $this->nameEN;
    }

    public function setNameEN(string $nameEN): self
    {
        $this->nameEN = $nameEN;

        return $this;
    }

    public function getDescriptionEN(): ?string
    {
        return $this->descriptionEN;
    }

    public function setDescriptionEN(string $descriptionEN): self
    {
        $this->descriptionEN = $descriptionEN;

        return $this;
    }

    public function getTitleEN(): ?string
    {
        return $this->titleEN;
    }

    public function setTitleEN(string $titleEN): self
    {
        $this->titleEN = $titleEN;

        return $this;
    }

    public function getNameNL(): ?string
    {
        return $this->nameNL;
    }

    public function setNameNL(string $nameNL): self
    {
        $this->nameNL = $nameNL;

        return $this;
    }

    public function getTitleNL(): ?string
    {
        return $this->titleNL;
    }

    public function setTitleNL(string $titleNL): self
    {
        $this->titleNL = $titleNL;

        return $this;
    }

    public function getDescriptionNL(): ?string
    {
        return $this->descriptionNL;
    }

    public function setDescriptionNL(string $descriptionNL): self
    {
        $this->descriptionNL = $descriptionNL;

        return $this;
    }

    public function getContentPrimaryEN(): ?string
    {
        return $this->contentPrimaryEN;
    }

    public function setContentPrimaryEN(?string $contentPrimaryEN): self
    {
        $this->contentPrimaryEN = $contentPrimaryEN;

        return $this;
    }

    public function getContentPrimaryNL(): ?string
    {
        return $this->contentPrimaryNL;
    }

    public function setContentPrimaryNL(?string $contentPrimaryNL): self
    {
        $this->contentPrimaryNL = $contentPrimaryNL;

        return $this;
    }

    public function getContentSecondaryEN(): ?string
    {
        return $this->contentSecondaryEN;
    }

    public function setContentSecondaryEN(?string $contentSecondaryEN): self
    {
        $this->contentSecondaryEN = $contentSecondaryEN;

        return $this;
    }

    public function getContentSecondaryNL(): ?string
    {
        return $this->contentSecondaryNL;
    }

    public function setContentSecondaryNL(?string $contentSecondaryNL): self
    {
        $this->contentSecondaryNL = $contentSecondaryNL;

        return $this;
    }

    public function getContentTertiaryEN(): ?string
    {
        return $this->contentTertiaryEN;
    }

    public function setContentTertiaryEN(?string $contentTertiaryEN): self
    {
        $this->contentTertiaryEN = $contentTertiaryEN;

        return $this;
    }

    public function getContentTertiaryNL(): ?string
    {
        return $this->contentTertiaryNL;
    }

    public function setContentTertiaryNL(?string $contentTertiaryNL): self
    {
        $this->contentTertiaryNL = $contentTertiaryNL;

        return $this;
    }

    public function getContentQuaternaryEN(): ?string
    {
        return $this->contentQuaternaryEN;
    }

    public function setContentQuaternaryEN(?string $contentQuaternaryEN): self
    {
        $this->contentQuaternaryEN = $contentQuaternaryEN;

        return $this;
    }

    public function getContentQuaternaryNL(): ?string
    {
        return $this->contentQuaternaryNL;
    }

    public function setContentQuaternaryNL(?string $contentQuaternaryNL): self
    {
        $this->contentQuaternaryNL = $contentQuaternaryNL;

        return $this;
    }

    public function getDevCodeRouteName(): ?string
    {
        return $this->devCodeRouteName;
    }

    public function setDevCodeRouteName(?string $devCodeRouteName): self
    {
        $this->devCodeRouteName = $devCodeRouteName;

        return $this;
    }

    public function getCtaTitle(): ?string
    {
        return $this->ctaTitle;
    }

    public function setCtaTitle(?string $ctaTitle): self
    {
        $this->ctaTitle = $ctaTitle;

        return $this;
    }

    public function getCtaText(): ?string
    {
        return $this->ctaText;
    }

    public function setCtaText(?string $ctaText): self
    {
        $this->ctaText = $ctaText;

        return $this;
    }

    public function getCtaTextEN(): ?string
    {
        return $this->ctaTextEN;
    }

    public function setCtaTextEN(?string $ctaTextEN): self
    {
        $this->ctaTextEN = $ctaTextEN;

        return $this;
    }

    public function getCtaTextNL(): ?string
    {
        return $this->ctaTextNL;
    }

    public function setCtaTextNL(?string $ctaTextNL): self
    {
        $this->ctaTextNL = $ctaTextNL;

        return $this;
    }

    public function getCtaUrl(): ?string
    {
        return $this->ctaUrl;
    }

    public function setCtaUrl(?string $ctaUrl): self
    {
        $this->ctaUrl = $ctaUrl;

        return $this;
    }

    public function getWeight(): ?int
    {
        return $this->weight;
    }

    public function setWeight(int $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function addChild(self $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children->add($child);
            $child->setParent($this);
        }

        return $this;
    }

    public function removeChild(self $child): self
    {
        if ($this->children->removeElement($child)) {
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }

    public function getBannerTitle(): ?string
    {
        return $this->bannerTitle;
    }

    public function setBannerTitle(?string $bannerTitle): self
    {
        $this->bannerTitle = $bannerTitle;

        return $this;
    }

    public function getBannerTitleEN(): ?string
    {
        return $this->bannerTitleEN;
    }

    public function setBannerTitleEN(?string $bannerTitleEN): self
    {
        $this->bannerTitleEN = $bannerTitleEN;

        return $this;
    }

    public function getBannerTitleNL(): ?string
    {
        return $this->bannerTitleNL;
    }

    public function setBannerTitleNL(?string $bannerTitleNL): self
    {
        $this->bannerTitleNL = $bannerTitleNL;

        return $this;
    }

    public function getCtaTitleEN(): ?string
    {
        return $this->ctaTitleEN;
    }

    public function setCtaTitleEN(?string $ctaTitleEN): self
    {
        $this->ctaTitleEN = $ctaTitleEN;

        return $this;
    }

    public function getCtaTitleNL(): ?string
    {
        return $this->ctaTitleNL;
    }

    public function setCtaTitleNL(?string $ctaTitleNL): self
    {
        $this->ctaTitleNL = $ctaTitleNL;

        return $this;
    }

    public function getBanner(): ?Image
    {
        return $this->banner;
    }

    public function setBanner(?Image $banner): self
    {
        $this->banner = $banner;

        return $this;
    }

    public function getImageThumbnail(): ?Image
    {
        return $this->ImageThumbnail;
    }

    public function setImageThumbnail(?Image $ImageThumbnail): self
    {
        $this->ImageThumbnail = $ImageThumbnail;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function isPageDeleted(): bool
    {
        if (null !== $this->getDeletedAt()) {
            return true;
        }

        return false;
    }

    public function getDevKey(): ?int
    {
        return $this->devKey;
    }

    public function setDevKey(?int $devKey): self
    {
        $this->devKey = $devKey;

        return $this;
    }

    /**
     * @return Collection<int, PageGallery>
     */
    public function getPageGalleries(): Collection
    {
        return $this->pageGalleries;
    }

    public function addPageGallery(PageGallery $pageGallery): self
    {
        if (!$this->pageGalleries->contains($pageGallery)) {
            $this->pageGalleries->add($pageGallery);
            $pageGallery->setPage($this);
        }

        return $this;
    }

    public function removePageGallery(PageGallery $pageGallery): self
    {
        if ($this->pageGalleries->removeElement($pageGallery)) {
            // set the owning side to null (unless already changed)
            if ($pageGallery->getPage() === $this) {
                $pageGallery->setPage(null);
            }
        }

        return $this;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'template' => $this->getTemplate(),
            'devKey' => $this->getDevKey(),
            'devCodeRouteName' => $this->getDevCodeRouteName(),
            'weight' => $this->getWeight(),
            'parent' => $this->getParent(),
            'children' => $this->getChildren(),
            'title' => $this->getTitle(),
            'titleEN' => $this->getTitleEN(),
            'titleNL' => $this->getTitleNL(),
            'name' => $this->getName(),
            'nameEN' => $this->getNameEN(),
            'nameNL' => $this->getNameNL(),
            'preDescription' => $this->getPreDescription(),
            'preDescriptionEN' => $this->getPreDescriptionEN(),
            'preDescriptionNL' => $this->getPreDescriptionNL(),
            'metaDescription' => $this->getMetaDescription(),
            'metaDescriptionEN' => $this->getMetaDescriptionEN(),
            'metaDescriptionNL' => $this->getMetaDescriptionNL(),
            'description' => $this->getDescription(),
            'descriptionEN' => $this->getDescriptionEN(),
            'descriptionNL' => $this->getDescriptionNL(),
            'contentPrimary' => $this->getContentPrimary(),
            'contentPrimaryEN' => $this->getContentPrimaryEN(),
            'contentPrimaryNL' => $this->getContentPrimaryNL(),
            'contentSecondary' => $this->getContentSecondary(),
            'contentSecondaryEN' => $this->getContentSecondaryEN(),
            'contentSecondaryNL' => $this->getContentSecondaryNL(),
            'contentTertiary' => $this->getContentTertiary(),
            'contentTertiaryEN' => $this->getContentTertiaryEN(),
            'contentTertiaryNL' => $this->getContentTertiaryNL(),
            'contentQuaternary' => $this->getContentQuaternary(),
            'contentQuaternaryEN' => $this->getContentQuaternaryEN(),
            'contentQuaternaryNL' => $this->getContentQuaternaryNL(),
            'ctaTitle' => $this->getCtaTitle(),
            'ctaTitleEN' => $this->getCtaTitleEN(),
            'ctaTitleNL' => $this->getCtaTitleNL(),
            'ctaText' => $this->getCtaText(),
            'ctaTextEN' => $this->getCtaTextEN(),
            'ctaTextNL' => $this->getCtaTextNL(),
            'ctaUrl' => $this->getCtaUrl(),
            'bannerTitle' => $this->getBannerTitle(),
            'bannerTitleEN' => $this->getBannerTitleEN(),
            'bannerTitleNL' => $this->getBannerTitleNL(),
            'banner' => $this->getBanner(),
            'imageThumbnail' => $this->getImageThumbnail(),
            'slug' => $this->getSlug(),
            'createdAt' => $this->getCreatedAt(),
            'updatedAt' => $this->getUpdatedAt(),
            'deletedAt' => $this->getDeletedAt(),
            'pageGalleries' => $this->getPageGalleries(),
            'category' => $this->getCategory(),
            'canonicalUrl' => $this->getCanonicalUrl(),
        ];
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(?string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getWebsite(): ?Website
    {
        return $this->website;
    }

    public function setWebsite(?Website $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(string $metaDescription): self
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    public function getMetaDescriptionEN(): ?string
    {
        return $this->metaDescriptionEN;
    }

    public function setMetaDescriptionEN(string $metaDescriptionEN): self
    {
        $this->metaDescriptionEN = $metaDescriptionEN;

        return $this;
    }

    public function getMetaDescriptionNL(): ?string
    {
        return $this->metaDescriptionNL;
    }

    public function setMetaDescriptionNL(string $metaDescriptionNL): self
    {
        $this->metaDescriptionNL = $metaDescriptionNL;

        return $this;
    }

    public function getCanonicalUrl(): ?string
    {
        return $this->canonicalUrl;
    }

    public function setCanonicalUrl(?string $canonicalUrl): self
    {
        $this->canonicalUrl = $canonicalUrl;

        return $this;
    }

    public function getContentPrimary(): ?string
    {
        return $this->contentPrimary;
    }

    public function setContentPrimary(?string $contentPrimary): self
    {
        $this->contentPrimary = $contentPrimary;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPreDescription(): ?string
    {
        return $this->preDescription;
    }

    public function setPreDescription(?string $preDescription): self
    {
        $this->preDescription = $preDescription;

        return $this;
    }

    public function getPreDescriptionEN(): ?string
    {
        return $this->preDescriptionEN;
    }

    public function setPreDescriptionEN(?string $preDescriptionEN): self
    {
        $this->preDescriptionEN = $preDescriptionEN;

        return $this;
    }

    public function getPreDescriptionNL(): ?string
    {
        return $this->preDescriptionNL;
    }

    public function setPreDescriptionNL(?string $preDescriptionNL): self
    {
        $this->preDescriptionNL = $preDescriptionNL;

        return $this;
    }

    public function getType(): ?PageType
    {
        return $this->type;
    }

    public function setType(?PageType $type): self
    {
        $this->type = $type;

        return $this;
    }
}
