<?php

/*
 * (c) No name
 */

namespace App\Entity;

use App\Repository\CountryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CountryRepository::class)]
class Country
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $alpha2 = null;

    #[ORM\Column]
    private ?int $code = null;

    #[ORM\Column(length: 255)]
    private ?string $alpha3 = null;

    #[ORM\Column(length: 255)]
    private ?string $nameEnGb = null;

    #[ORM\Column(length: 255)]
    private ?string $nameFrFr = null;

    #[ORM\OneToMany(mappedBy: 'country', targetEntity: UserBackend::class)]
    private Collection $users;

    #[ORM\ManyToOne(inversedBy: 'countries')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Timezone $timezone = null;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAlpha2(): ?string
    {
        return $this->alpha2;
    }

    public function setAlpha2(string $alpha2): self
    {
        $this->alpha2 = $alpha2;

        return $this;
    }

    public function getCode(): ?int
    {
        return $this->code;
    }

    public function setCode(int $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getAlpha3(): ?string
    {
        return $this->alpha3;
    }

    public function setAlpha3(string $alpha3): self
    {
        $this->alpha3 = $alpha3;

        return $this;
    }

    public function getNameEnGb(): ?string
    {
        return $this->nameEnGb;
    }

    public function setNameEnGb(string $nameEnGb): self
    {
        $this->nameEnGb = $nameEnGb;

        return $this;
    }

    public function getNameFrFr(): ?string
    {
        return $this->nameFrFr;
    }

    public function setNameFrFr(string $nameFrFr): self
    {
        $this->nameFrFr = $nameFrFr;

        return $this;
    }

    /**
     * @return Collection<int, UserBackend>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(UserBackend $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->setCountry($this);
        }

        return $this;
    }

    public function removeUser(UserBackend $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getCountry() === $this) {
                $user->setCountry(null);
            }
        }

        return $this;
    }

    public function getTimezone(): ?Timezone
    {
        return $this->timezone;
    }

    public function setTimezone(?Timezone $timezone): self
    {
        $this->timezone = $timezone;

        return $this;
    }

    public function __toString(): string
    {
        return $this->nameEnGb;
    }
}
