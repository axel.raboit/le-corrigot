<?php

declare(strict_types=1);

/*
 * (c) No name
 */

namespace App\Manager;

use App\Entity\DataEnum;
use App\Entity\Page;
use App\Repository\DataEnumRepository;
use App\Repository\PageRepository;
use Doctrine\ORM\EntityManagerInterface;

class DataEnumManager extends BaseManager
{
    private DataEnumRepository $dataEnumRepository;
    private PageRepository $PageRepository;
    protected $em;

    public function __construct(
        EntityManagerInterface $entityManager,
        DataEnumRepository $dataEnumRepository,
        PageRepository $PageRepository
    ) {
        $this->dataEnumRepository = $dataEnumRepository;
        $this->PageRepository = $PageRepository;
        $this->em = $entityManager;
    }

    /**
     * @return string|bool|int|null
     */
    public function getDataEnumValue(
        int $dataEnum
    ) {
        /* $dataEnum = mb_strtoupper($dataEnum); */

        $dataEnum = $this->dataEnumRepository->findOneBy(['devKey' => $dataEnum]);

        if (!$dataEnum) {
            throw new \Exception('DataEnum not found');
        }

        $value = $dataEnum->getValue();

        if ('false' === $value || 'true' === $value) {
            $value = 'true' === $value;
        }

        return $value;
    }

    public function getPagebyDataDevKey(
        int $pageDevKey
    ): Page {
        return $this->PageRepository->findOneBy(['devKey' => $pageDevKey]);
    }

    public function createFromArray(
        array $params = []
    ): DataEnum {
        $dataEnum = new DataEnum();

        // ID
        if (\array_key_exists('id', $params)) {
            $dataEnum->setId($params['id']);
        }

        // Name
        if (\array_key_exists('name', $params)) {
            $dataEnum->setName($params['name']);
        }

        // Category
        if (\array_key_exists('category', $params)) {
            $dataEnum->setCategory($params['category']);
        }

        // Value
        if (\array_key_exists('value', $params)) {
            $dataEnum->setValue($params['value']);
        }

        // DevKey
        if (\array_key_exists('dev_key', $params)) {
            $dataEnum->setDevKey($params['dev_key']);
        }

        return $this->save($dataEnum);
    }

    public function save(
        DataEnum $dataEnum
    ): DataEnum {
        $em = $this->em();
        $em->persist($dataEnum);
        $em->flush();

        return $dataEnum;
    }

    // BASE

    public function em(
        bool $refresh = false
    ): EntityManagerInterface {
        if (false === $this->em->getConnection()->isConnected()) {
            $this->em->getConnection()->close();
            $this->em->getConnection()->connect();
        }

        return $this->em;
    }
}
