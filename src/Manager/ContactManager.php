<?php

declare(strict_types=1);

/*
 * (c) No name
 */

namespace App\Manager;

use App\Entity\Contact;
use Doctrine\ORM\EntityManagerInterface;

class ContactManager extends BaseManager
{
    protected $em;

    public function __construct(
        EntityManagerInterface $entityManager,
    ) {
        $this->em = $entityManager;
    }

    public function save(
        Contact $contact
    ): Contact {
        $em = $this->em();
        $em->persist($contact);
        $em->flush();

        return $contact;
    }

    // BASE

    public function em(
        bool $refresh = false
    ): EntityManagerInterface {
        if (false === $this->em->getConnection()->isConnected()) {
            $this->em->getConnection()->close();
            $this->em->getConnection()->connect();
        }

        return $this->em;
    }
}
