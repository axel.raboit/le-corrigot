<?php

declare(strict_types=1);

/*
 * (c) No name
 */

namespace App\Enum;

class DataEnum
{
    // Last used devKey: #1021

    public const DATA_PAGE_CONTACT_DEV_KEY = 10001;
    public const DATA_PAGE_BACKEND_LOGIN_DEV_KEY = 10002;
    public const DATA_PAGE_BACKEND_REGISTER_DEV_KEY = 10003;
    public const DATA_PAGE_OUTDOORS_DEV_KEY = 10004;
    public const DATA_RECAPTCHA_SECRET_DEV_KEY = 10005;
    public const DATA_RECAPTCHA_PUBLIC_DEV_KEY = 10006;
    public const DATA_FRONTEND_MENU_DEV_KEY = 10007;
    public const DATA_FRONTEND_FOOTER_DEV_KEY = 10008;
    public const DATA_APPLICATION_MENU_DEV_KEY = 10009;
    public const DATA_ADMIN_MENU_DEV_KEY = 10010;
    public const DATA_PAGE_ROOMS_DEV_KEY = 10011;
    public const DATA_PAGE_JOIN_US_DEV_KEY = 10012;
    public const DATA_TEMPLATE_EMAIL_CONTACT_DEV_KEY = 10013;
    public const DATA_PAGE_CONTACT_PRIVACY_POLICY_DEV_KEY = 10014;
    public const DATA_PAGE_REGISTRATION_POLICY_ACCEPTANCE_REGISTRATION_DEV_KEY = 10016;
    public const DATA_BACKEND_REGISTRATION_ACTIVATION_DEV_KEY = 10017;
    public const DATA_BACKEND_LOGIN_ACTIVATION_DEV_KEY = 10018;
    public const DATA_PAGE_ERROR_404_DEV_KEY = 10019;
    public const DATA_LELOFT_FRONTEND_MENU_DEV_KEY = 10020;
    public const DATA_MAINTENANCE_FRONTEND_ACTIVATION_DEV_KEY = 10021;

    /**
     * @var array
     */
    public static $data = [
        self::DATA_MAINTENANCE_FRONTEND_ACTIVATION_DEV_KEY => [
            'name' => 'DATA_MAINTENANCE_FRONTEND_ACTIVATION_DEV_KEY',
            'dev_key' => self::DATA_MAINTENANCE_FRONTEND_ACTIVATION_DEV_KEY,
            'value' => 'false',
            'category' => 'maintenance',
        ],
        self::DATA_PAGE_ERROR_404_DEV_KEY => [
            'name' => 'DATA_PAGE_ERROR_404_DEV_KEY',
            'dev_key' => self::DATA_PAGE_ERROR_404_DEV_KEY,
            'value' => null,
            'category' => 'page',
        ],
        self::DATA_PAGE_REGISTRATION_POLICY_ACCEPTANCE_REGISTRATION_DEV_KEY => [
            'name' => 'DATA_PAGE_REGISTRATION_POLICY_ACCEPTANCE_REGISTRATION_DEV_KEY',
            'dev_key' => self::DATA_PAGE_REGISTRATION_POLICY_ACCEPTANCE_REGISTRATION_DEV_KEY,
            'value' => null,
            'category' => 'page',
        ],
        self::DATA_PAGE_CONTACT_PRIVACY_POLICY_DEV_KEY => [
            'name' => 'DATA_PAGE_CONTACT_PRIVACY_POLICY_DEV_KEY',
            'dev_key' => self::DATA_PAGE_CONTACT_PRIVACY_POLICY_DEV_KEY,
            'value' => null,
            'category' => 'page',
        ],
        self::DATA_TEMPLATE_EMAIL_CONTACT_DEV_KEY => [
            'name' => 'DATA_TEMPLATE_EMAIL_CONTACT_DEV_KEY',
            'dev_key' => self::DATA_TEMPLATE_EMAIL_CONTACT_DEV_KEY,
            'value' => 'email/template/default-contact.html.twig',
            'category' => 'email',
        ],
        self::DATA_BACKEND_REGISTRATION_ACTIVATION_DEV_KEY => [
            'name' => 'DATA_BACKEND_REGISTRATION_ACTIVATION_DEV_KEY',
            'dev_key' => self::DATA_BACKEND_REGISTRATION_ACTIVATION_DEV_KEY,
            'value' => 'false',
            'category' => 'user',
        ],
        self::DATA_BACKEND_LOGIN_ACTIVATION_DEV_KEY => [
            'name' => 'DATA_BACKEND_LOGIN_ACTIVATION_DEV_KEY',
            'dev_key' => self::DATA_BACKEND_LOGIN_ACTIVATION_DEV_KEY,
            'value' => 'false',
            'category' => 'user',
        ],
        self::DATA_RECAPTCHA_SECRET_DEV_KEY => [
            'name' => 'DATA_RECAPTCHA_SECRET_DEV_KEY',
            'dev_key' => self::DATA_RECAPTCHA_SECRET_DEV_KEY,
            'value' => null,
            'category' => 'google',
        ],
        self::DATA_RECAPTCHA_PUBLIC_DEV_KEY => [
            'name' => 'DATA_RECAPTCHA_PUBLIC_DEV_KEY',
            'dev_key' => self::DATA_RECAPTCHA_PUBLIC_DEV_KEY,
            'value' => null,
            'category' => 'google',
        ],
        self::DATA_FRONTEND_MENU_DEV_KEY => [
            'name' => 'DATA_FRONTEND_MENU_DEV_KEY',
            'dev_key' => self::DATA_FRONTEND_MENU_DEV_KEY,
            'value' => 'frontend',
            'category' => 'menu',
        ],
        self::DATA_LELOFT_FRONTEND_MENU_DEV_KEY => [
            'name' => 'DATA_LELOFT_FRONTEND_MENU_DEV_KEY',
            'dev_key' => self::DATA_LELOFT_FRONTEND_MENU_DEV_KEY,
            'value' => 'leloft frontend',
            'category' => 'menu',
        ],
        self::DATA_FRONTEND_FOOTER_DEV_KEY => [
            'name' => 'DATA_FRONTEND_FOOTER_DEV_KEY',
            'dev_key' => self::DATA_FRONTEND_FOOTER_DEV_KEY,
            'value' => 'footer',
            'category' => 'footer',
        ],
        self::DATA_APPLICATION_MENU_DEV_KEY => [
            'name' => 'DATA_APPLICATION_MENU_DEV_KEY',
            'dev_key' => self::DATA_APPLICATION_MENU_DEV_KEY,
            'value' => 'application',
            'category' => 'menu',
        ],
        self::DATA_ADMIN_MENU_DEV_KEY => [
            'name' => 'DATA_ADMIN_MENU_DEV_KEY',
            'dev_key' => self::DATA_ADMIN_MENU_DEV_KEY,
            'value' => 'admin',
            'category' => 'menu',
        ],
        self::DATA_PAGE_CONTACT_DEV_KEY => [
            'name' => 'DATA_PAGE_CONTACT_DEV_KEY',
            'dev_key' => self::DATA_PAGE_CONTACT_DEV_KEY,
            'value' => null,
            'category' => 'page',
        ],
        self::DATA_PAGE_BACKEND_LOGIN_DEV_KEY => [
            'name' => 'DATA_PAGE_BACKEND_LOGIN_DEV_KEY',
            'dev_key' => self::DATA_PAGE_BACKEND_LOGIN_DEV_KEY,
            'value' => null,
            'category' => 'page',
        ],
        self::DATA_PAGE_BACKEND_REGISTER_DEV_KEY => [
            'name' => 'DATA_PAGE_BACKEND_REGISTER_DEV_KEY',
            'dev_key' => self::DATA_PAGE_BACKEND_REGISTER_DEV_KEY,
            'value' => null,
            'category' => 'page',
        ],
        self::DATA_PAGE_OUTDOORS_DEV_KEY => [
            'name' => 'DATA_PAGE_OUTDOORS_DEV_KEY',
            'dev_key' => self::DATA_PAGE_OUTDOORS_DEV_KEY,
            'value' => null,
            'category' => 'page',
        ],
        self::DATA_PAGE_ROOMS_DEV_KEY => [
            'name' => 'DATA_PAGE_ROOMS_DEV_KEY',
            'dev_key' => self::DATA_PAGE_ROOMS_DEV_KEY,
            'value' => null,
            'category' => 'page',
        ],
        self::DATA_PAGE_JOIN_US_DEV_KEY => [
            'name' => 'DATA_PAGE_JOIN_US_DEV_KEY',
            'dev_key' => self::DATA_PAGE_JOIN_US_DEV_KEY,
            'value' => null,
            'category' => 'page',
        ],
    ];

    public static function getAvailable(): array
    {
        $reflectionClass = new \ReflectionClass(self::class);

        return array_values((array) $reflectionClass->getConstants());
    }

    public static function getConstants(): array
    {
        $reflectionClass = new \ReflectionClass(self::class);

        return (array) $reflectionClass->getConstants();
    }
}
