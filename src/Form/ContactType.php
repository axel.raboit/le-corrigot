<?php

declare(strict_types=1);

/*
 * (c) No name
 */

namespace App\Form;

use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $lang = $options['lang'];

        $translation = [
            'fr' => [
                'name' => [
                    'message' => 'Veuillez renseigner votre nom',
                    'minMessage' => 'Votre nom doit comporter au moins {{ limit }} caractères',
                    'maxMessage' => 'Votre nom ne peut pas comporter plus de {{ limit }} caractères',
                ],
                'phone' => [
                    'message' => 'Veuillez renseigner votre numéro de téléphone',
                    'minMessage' => 'Votre numéro de téléphone doit comporter au moins {{ limit }} caractères',
                    'maxMessage' => 'Votre numéro de téléphone ne peut pas comporter plus de {{ limit }} caractères',
                ],
                'email' => [
                    'emptyMessage' => 'Veuillez renseigner votre adresse email',
                    'regexMessage' => 'Veuillez renseigner une adresse email valide',
                ],
                'subject' => [
                    'message' => 'Veuillez renseigner votre numéro de téléphone',
                    'minMessage' => 'Votre sujet doit comporter au moins {{ limit }} caractères',
                    'maxMessage' => 'Votre sujet ne peut pas comporter plus de {{ limit }} caractères',
                ],
                'message' => [
                    'message' => 'Veuillez renseigner votre numéro de téléphone',
                    'minMessage' => 'Votre message doit comporter au moins {{ limit }} caractères',
                    'maxMessage' => 'Votre message ne peut pas comporter plus de {{ limit }} caractères',
                ],
                'policyPrivacy' => [
                    'message' => 'Veuillez accepter la politique de confidentialité',
                ],
            ],
            'en' => [
                'name' => [
                    'message' => 'Please enter your name',
                    'minMessage' => 'Your name must contain at least {{ limit }} characters',
                    'maxMessage' => 'Your name cannot contain more than {{ limit }} characters',
                ],
                'phone' => [
                    'message' => 'Please enter your phone number',
                    'minMessage' => 'Your phone number must contain at least {{ limit }} characters',
                    'maxMessage' => 'Your phone number cannot contain more than {{ limit }} characters',
                ],
                'email' => [
                    'emptyMessage' => 'Please enter your email address',
                    'regexMessage' => 'Please enter a valid email address',
                ],
                'subject' => [
                    'message' => 'Please enter a subject',
                    'minMessage' => 'Subject must contain at least {{ limit }} characters',
                    'maxMessage' => 'Subject cannot contain more than {{ limit }} characters',
                ],
                'message' => [
                    'message' => 'Please enter a message',
                    'minMessage' => 'Message must contain at least {{ limit }} characters',
                    'maxMessage' => 'Message cannot contain more than {{ limit }} characters',
                ],
                'policyPrivacy' => [
                    'message' => 'Please accept the privacy policy',
                ],
            ],
            'nl' => [
                'name' => [
                    'message' => 'Bitte geben Sie Ihren Namen ein',
                    'minMessage' => 'Ihr Name muss mindestens {{ limit }} Zeichen enthalten',
                    'maxMessage' => 'Ihr Name darf nicht mehr als {{ limit }} Zeichen enthalten',
                ],
                'phone' => [
                    'message' => 'Bitte geben Sie Ihre Telefonnummer ein',
                    'minMessage' => 'Ihre Telefonnummer muss mindestens {{ limit }} Zeichen enthalten',
                    'maxMessage' => 'Ihre Telefonnummer darf nicht mehr als {{ limit }} Zeichen enthalten',
                ],
                'email' => [
                    'emptyMessage' => 'Bitte geben Sie Ihre E-Mail-Adresse ein',
                    'regexMessage' => 'Bitte geben Sie eine gültige E-Mail-Adresse ein',
                ],
                'subject' => [
                    'message' => 'Bitte geben Sie einen Betreff ein',
                    'minMessage' => 'Betreff muss mindestens {{ limit }} Zeichen enthalten',
                    'maxMessage' => 'Betreff darf nicht mehr als {{ limit }} Zeichen enthalten',
                ],
                'message' => [
                    'message' => 'Bitte geben Sie eine Nachricht ein',
                    'minMessage' => 'Nachricht muss mindestens {{ limit }} Zeichen enthalten',
                    'maxMessage' => 'Nachricht darf nicht mehr als {{ limit }} Zeichen enthalten',
                ],
                'policyPrivacy' => [
                    'message' => 'Bitte akzeptieren Sie die Datenschutzrichtlinie',
                ],
            ],
        ];

        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'label' => false,
                'constraints' => [
                    new NotBlank(['message' => $translation[$lang]['name']['message']]),
                    new Length([
                        'min' => 2,
                        'minMessage' => $translation[$lang]['name']['minMessage'],
                        'max' => 30,
                        'maxMessage' => $translation[$lang]['name']['maxMessage'],
                    ]),
                ],
            ])
            ->add('address', TextType::class, [
                'required' => false,
                'label' => false,
            ])
            ->add('city', TextType::class, [
                'required' => false,
                'label' => false,
            ])
            ->add('postalCode', TextType::class, [
                'required' => false,
                'label' => false,
            ])
            ->add('country', TextType::class, [
                'required' => false,
                'label' => false,
            ])
            ->add('phone', TextType::class, [
                'required' => true,
                'label' => false,
                'constraints' => [
                    new NotBlank(['message' => $translation[$lang]['phone']['message']]),
                    new Length([
                        'min' => 2,
                        'minMessage' => $translation[$lang]['phone']['minMessage'],
                        'max' => 12,
                        'maxMessage' => $translation[$lang]['phone']['maxMessage'],
                    ]),
                ],
            ])
            ->add('email', EmailType::class, [
                'required' => true,
                'label' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => $translation[$lang]['email']['emptyMessage'],
                    ]),
                    new Email(),
                    new Regex([
                        'pattern' => '/^[a-zA-Z][a-zA-Z0-9._\/-]{1,255}@[a-z]{4,150}\.[a-z]{2,3}$/',
                        'message' => $translation[$lang]['email']['regexMessage'],
                    ]),
                ],
            ])
            ->add('subject', TextType::class, [
                'required' => true,
                'label' => false,
                'constraints' => [
                    new NotBlank(['message' => $translation[$lang]['subject']['message']]),
                    new Length([
                        'min' => 2,
                        'minMessage' => $translation[$lang]['subject']['minMessage'],
                        'max' => 50,
                        'maxMessage' => $translation[$lang]['subject']['maxMessage'],
                    ]),
                ],
            ])
            ->add('message', TextareaType::class, [
                'required' => true,
                'label' => false,
                'constraints' => [
                    new NotBlank(['message' => $translation[$lang]['message']['message']]),
                    new Length([
                        'min' => 2,
                        'minMessage' => $translation[$lang]['message']['minMessage'],
                        'max' => 1500,
                        'maxMessage' => $translation[$lang]['message']['maxMessage'],
                    ]),
                ],
            ])
            ->add('policyPolicy', CheckboxType::class, [
                'mapped' => false,
                'required' => true,
                'label' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => $translation[$lang]['policyPrivacy']['message'],
                    ]),
                ],
            ])
            ->add('recaptcha', HiddenType::class, [
                'mapped' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
            'lang' => '',
        ]);
    }
}
