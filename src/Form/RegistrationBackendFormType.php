<?php

declare(strict_types=1);

/*
 * (c) No name
 */

namespace App\Form;

use App\Entity\UserBackend;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class RegistrationBackendFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $lang = $options['lang'];

        $translation = [
            'fr' => [
                'email' => [
                    'message' => 'Vous devez renseigner un email.',
                    'regex_message' => 'Votre email n\'est pas valide.',
                ],
                'agree' => [
                    'message' => 'Vous devez accepter les conditions.',
                ],
                'plainPassword' => [
                    'message' => 'Veuillez entrer un mot de passe.',
                    'regex_message' => 'Le mot de passe doit contenir au moins un chiffre, une minuscule et une majuscule, et un caractère spécial.',
                    'minMessage' => 'Votre mot de passe doit comporter au moins {{ limit }} caractères.',
                    'maxMessage' => 'Votre mot de passe ne peut pas comporter plus de {{ limit }} caractères.',
                ],
                'timezone' => [
                    'placeholder' => 'Choisissez votre pays',
                    'message' => 'Vous devez choisir un pays.',
                ],
            ],
            'en' => [
                'email' => [
                    'message' => 'You must fill in an email.',
                    'regex_message' => 'Your email is not valid.',
                ],
                'agree' => [
                    'message' => 'You should agree terms.',
                ],
                'plainPassword' => [
                    'message' => 'Please enter a password.',
                    'regex_message' => 'Password must contain at least one digit, one lowercase and one uppercase letter, and one special character.',
                    'minMessage' => 'Your password must contain at least {{ limit }} characters.',
                    'maxMessage' => 'Your password cannot contain more than {{ limit }} characters.',
                ],
                'timezone' => [
                    'placeholder' => 'Choose your country',
                    'message' => 'You must choose a country.',
                ],
            ],
            'nl' => [
                'email' => [
                    'message' => 'U moet een e-mail invullen.',
                    'regex_message' => 'Uw e-mail is niet geldig.',
                ],
                'agree' => [
                    'message' => 'U moet de voorwaarden accepteren.',
                ],
                'plainPassword' => [
                    'message' => 'Vul een wachtwoord in.',
                    'regex_message' => 'Wachtwoord moet ten minste één cijfer, één kleine letter en één hoofdletter en één speciaal teken bevatten.',
                    'minMessage' => 'Uw wachtwoord moet ten minste {{ limit }} tekens bevatten.',
                    'maxMessage' => 'Uw wachtwoord mag niet meer dan {{ limit }} tekens bevatten.',
                ],
                'timezone' => [
                    'placeholder' => 'Kies uw land',
                    'message' => 'U moet een land kiezen.',
                ],
            ],
        ];

        $builder
            ->add('email', EmailType::class, [
                'label' => 'form.register.field.email',
                'constraints' => [
                    new NotBlank([
                        'message' => $translation[$lang]['email']['message'],
                    ]),
                    new Regex([
                        'pattern' => '/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/',
                        'message' => $translation[$lang]['email']['regex_message'],
                    ]),
                ],
            ])
            ->add('country', null, [
                'label' => 'form.register.field.country',
                'placeholder' => $translation[$lang]['timezone']['placeholder'],
                'constraints' => [
                    new NotBlank([
                        'message' => $translation[$lang]['timezone']['message'],
                    ]),
                ],
            ])
            ->add('agreeTerms', CheckboxType::class, [
                'label' => false,
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => $translation[$lang]['agree']['message'],
                    ]),
                ],
            ])
            ->add('plainPassword', PasswordType::class, [
                'label' => 'form.register.field.password',
                'mapped' => false,
                'attr' => ['autocomplete' => 'new-password'],
                'constraints' => [
                    new NotBlank([
                        'message' => $translation[$lang]['plainPassword']['message'],
                    ]),
                    new Length([
                        'min' => 14,
                        'minMessage' => $translation[$lang]['plainPassword']['minMessage'],
                        'max' => 4096,
                        'maxMessage' => $translation[$lang]['plainPassword']['maxMessage'],
                    ]),
                    new Regex([
                        'pattern' => '/^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z\\d]).{14,}$/',
                        'message' => $translation[$lang]['plainPassword']['regex_message'],
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => UserBackend::class,
            'lang' => '',
        ]);
    }
}
