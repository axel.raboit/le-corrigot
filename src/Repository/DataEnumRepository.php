<?php

declare(strict_types=1);

/*
 * (c) No name
 */

namespace App\Repository;

use App\Entity\DataEnum;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DataEnum>
 *
 * @method DataEnum|null find($id, $lockMode = null, $lockVersion = null)
 * @method DataEnum|null findOneBy(array $criteria, array $orderBy = null)
 * @method DataEnum[]    findAll()
 * @method DataEnum[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DataEnumRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DataEnum::class);
    }

    public function getLastDevKey(): int
    {
        $query = $this->getEntityManager()->createQuery(
            'SELECT MAX(d.devKey) FROM App\Entity\DataEnum d'
        );

        return $query->getSingleScalarResult();
    }

    public function save(DataEnum $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(DataEnum $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
