<?php

/*
 * (c) No name
 */

namespace App\Repository;

use App\Entity\IpWhitelist;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<IpWhitelist>
 *
 * @method IpWhitelist|null find($id, $lockMode = null, $lockVersion = null)
 * @method IpWhitelist|null findOneBy(array $criteria, array $orderBy = null)
 * @method IpWhitelist[]    findAll()
 * @method IpWhitelist[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IpWhitelistRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IpWhitelist::class);
    }

    public function save(IpWhitelist $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(IpWhitelist $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findIp(string $ip): bool
    {
        $result = $this->findOneBy(['ip' => $ip]);

        return null !== $result;
    }
}
