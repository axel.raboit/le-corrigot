<?php

declare(strict_types=1);

/*
 * (c) No name
 */

namespace App\Security\Voter;

use App\Entity\UserBackend;
use App\Enum\DataEnum;
use App\Manager\DataEnumManager;
use App\Service\ClientIpService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class MaintenanceVoter extends Voter
{
    public const CAN_ACCESS_WHEN_MAINTENANCE_IS_ACTIVATED = 'CAN_ACCESS_WHEN_MAINTENANCE_IS_ACTIVATED';

    private DataEnumManager $dataEnumManager;
    private ClientIpService $clientIpService;

    public function __construct(
        ClientIpService $clientIpService,
        DataEnumManager $dataEnumManager,
    ) {
        $this->clientIpService = $clientIpService;
        $this->dataEnumManager = $dataEnumManager;
    }

    protected function supports(
        string $attribute,
        mixed $subject
    ): bool {
        // Control attribute
        if (!\in_array($attribute, [self::CAN_ACCESS_WHEN_MAINTENANCE_IS_ACTIVATED], true)) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(
        string $attribute,
        mixed $subject,
        TokenInterface $token
    ): bool {
        /** @var UserBackend $user */
        $user = $token->getUser();

        switch ($attribute) {
            case self::CAN_ACCESS_WHEN_MAINTENANCE_IS_ACTIVATED:
                return $this->canAccessWhenMaintenanceIsActivated($user);
        }

        throw new \LogicException('This code should not be reached.');
    }

    private function canAccessWhenMaintenanceIsActivated(?UserBackend $user): bool
    {
        $maintenanceIsActivated = $this->dataEnumManager->getDataEnumValue(DataEnum::DATA_MAINTENANCE_FRONTEND_ACTIVATION_DEV_KEY);

        return !$maintenanceIsActivated || $this->clientIpService->clientIpIsAllowed() || $this->isLogged($user);
    }

    // BASE

    private function isLogged(
        ?UserBackend $user
    ): bool {
        return null !== $user;
    }
}
