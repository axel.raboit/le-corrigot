<?php

/*
 * (c) No name
 */

namespace App\EventSubscriber;

use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityUpdatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class EasyAdminSubscriber implements EventSubscriberInterface
{
    /* private $entityManager;

    private $passwordEncoder; */

    /* public function __construct(EntityManagerInterface $entityManager, UserPasswordHasherInterface $userPasswordHasher)
    {
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $userPasswordHasher;
    } */

    public static function getSubscribedEvents()
    {
        return [
            /* BeforeEntityPersistedEvent::class => ['addUser'], */
            /* BeforeEntityUpdatedEvent::class => ['updateUser'], */
        ];
    }

    /* public function updateUser(BeforeEntityUpdatedEvent $event): void
    {
        $entity = $event->getEntityInstance();

        if (!($entity instanceof User)) {
            return;
        }

        if (!$entity->getPassword()) {
            $user = $this->em()->getRepository(User::class)->find($entity->getId());
            $entity->setPassword($user->getPassword());
        }

        $this->setPassword($entity);
    } */

    /* public function addUser(mixed $event): void
    {
        if (!$event instanceof BeforeEntityPersistedEvent) {
            return;
        }

        $entity = $event->getEntityInstance();

        $entity->setRoles(['ROLE_BACKEND']);
        $this->setPassword($entity);

        $entityManager = $this->em();
        $entityManager->persist($entity);
        $entityManager->flush();
    } */

    /* public function test(BeforeEntityPersistedEvent $event): void
    {
        $entity = $event->getEntityInstance();

        if (null === $entity->getPassword()) {
            $user = $this->em()->getRepository(User::class)->find($entity->getId());
            $entity->setPassword($user->getPassword());
        }

        $entityManager = $this->em();
        $entityManager->persist($entity);
        $entityManager->flush();
    } */

    /* public function setPassword(User $entity): void
    {
        $pass = $entity->getPassword();

        $entity->setPassword(
            $this->passwordEncoder->hashPassword(
                $entity,
                $pass
            )
        );
    } */

    // BASE

    /* public function em(
        bool $refresh = false
    ): EntityManagerInterface {
        if (false === $this->entityManager->getConnection()->isConnected()) {
            $this->entityManager->getConnection()->close();
            $this->entityManager->getConnection()->connect();
        }

        return $this->entityManager;
    } */
}
