const contactName = document.getElementById('contact_name');
const contactPhone = document.getElementById('contact_phone');
const contactEmail = document.getElementById('contact_email');
const contactSubject = document.getElementById('contact_subject');
const contactMessage = document.getElementById('contact_message');
const inputs = [contactName, contactPhone, contactEmail, contactSubject, contactMessage];

const localeLang = document.getElementById('locale-lang').getAttribute('data-lang');

function isEmpty(input) {
    input.addEventListener('focusout', function () {
        if (input.value === '') {
            const parent = input.parentElement;

            if (!parent.querySelector('.form-error')) {
                const ul = document.createElement('ul');
                ul.classList.add('form-error', 'p-0', 'm-0');
    
                if (localeLang === 'nl') {
                    ul.innerHTML = '<li class="list-unstyled">Dieses Feld ist erforderlich.</li>';
                } else if (localeLang === 'en') {
                    ul.innerHTML = '<li class="list-unstyled">This field is required.</li>';
                } else {
                    ul.innerHTML = '<li class="list-unstyled">Ce champ est requis.</li>';
                }
                
                parent.appendChild(ul);
            }
        }

        if (input.value !== '') {
            const parent = input.parentElement;
            const ul = parent.querySelector('.form-error');

            if (ul) {
                parent.removeChild(ul);
            }
        }
    });
}

inputs.forEach(function (input) {
    isEmpty(input);
});